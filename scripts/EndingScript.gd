extends Node2D

const OBJ_FADEOUT = preload("res://objects/FadeOut.tscn")
const OBJ_TRANSITION = preload("res://objects/ui/Transition.tscn")

export (NodePath) var path_carl

onready var carl = get_node(path_carl)

func back_to_map():
	var transition = OBJ_TRANSITION.instance()
	transition.transition_type = 1
	transition.destination_scene = "res://scenes/Map.tscn"
	add_child(transition)

func go_to_credits():
	get_parent().fade_out_ambience()
	var transition = OBJ_FADEOUT.instance()
	# If this is the first time beating the game, go to the unlock screen:
	if GameProgress.game_finished:
		transition.destination_scene = "res://scenes/TitleScreen.tscn"
	else:
		GameProgress.game_finished = true
		GameProgress.save_game()
		transition.destination_scene = "res://scenes/SpeedrunUnlocked.tscn"
	add_child(transition)
	transition.fade_out()

func _ready():
	if GameProgress.get_curses_cleared() >= GameProgress.get_total_curses():
		carl.conversation_slug = "true_ending"
		carl.connect("conversation_finished", self, "go_to_credits")
	else:
		carl.conversation_slug = "fake_ending"
		carl.connect("conversation_finished", self, "back_to_map")
