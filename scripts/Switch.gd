extends Area2D

onready var sprite = $Sprite
onready var sprite_indicator = $Indicator
onready var audio = $Audio_Press

var pressed = false

onready var animation_index = 0

export (int) var indicator_index

signal pressed

func _physics_process(delta):
	animation_index += delta
	if pressed:
		sprite.frame = 1 + (int(animation_index * 30) % 2)
	else:
		sprite.frame = 0

func press():
	pressed = true

func depress():
	pressed = false

func interact():
	audio.play()
	emit_signal("pressed")
	if pressed:
		depress()
	else:
		press()

func reset():
	pressed = false

func _ready():
	sprite_indicator.frame = indicator_index
