extends StaticBody2D

const ANIM_SPEED = 30

export (float) var starting_delay = 2
export (float) var delay_while_on = 2
export (float) var delay_while_off = 4

export (bool) var muted = false

onready var sprite = $Sprite
onready var warning = $Warning
onready var collision = $CollisionShape2D
onready var timer = $Timer
onready var audio_appear = $Audio_Appear
onready var audio_disappear = $Audio_Disappear

onready var solid = false

onready var target_frame = 0
onready var actual_frame = 0

func _physics_process(delta):
	if target_frame > actual_frame:
		actual_frame += delta * ANIM_SPEED
		sprite.frame = int(round(actual_frame))
	elif target_frame < actual_frame:
		actual_frame -= delta * ANIM_SPEED
		sprite.frame = int(round(actual_frame))

func _on_Timer_timeout():
	if solid:
		#actual_frame = sprite.hframes - 1
		target_frame = 0
		collision.disabled = true
		solid = false
		timer.start(delay_while_off)
		if not muted:
			audio_disappear.play()
		if Curses.get_current_curse() == Curses.CANNOT_LAND:
			warning.hide()
	else:
		#actual_frame = 0
		target_frame = sprite.hframes - 1
		collision.disabled = false
		solid = true
		timer.start(delay_while_on)
		if not muted:
			audio_appear.play()
		if Curses.get_current_curse() == Curses.CANNOT_LAND:
			warning.show()

func reset():
	collision.disabled = true
	target_frame = 0
	actual_frame = 0
	sprite.frame = 0
	solid = false
	timer.start(starting_delay)
	warning.hide()

func _ready():
	collision.disabled = true
	target_frame = 0
	actual_frame = 0
	sprite.frame = 0
	solid = false
	timer.start(starting_delay)
