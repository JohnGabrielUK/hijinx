tool
extends Area2D

class_name CameraZone

var id : int
var corner : Position2D

func _on_CameraZone_body_entered(body) -> void:
	if body is Player:
		body.set_camera_bounds(id, global_position, corner.global_position)

func _on_CameraZone_body_exited(body) -> void:
	if body is Player:
		body.unset_camera_bounds(id)

func _ready() -> void:
	for current_body in get_overlapping_bodies():
		if current_body is Player:
			current_body.set_camera_bounds(id, global_position, corner.global_position)

func _enter_tree() -> void:
	id = get_instance_id()
	for current_child in get_children():
		if current_child is Position2D:
			corner = current_child

func _get_configuration_warning() -> String:
	if corner == null:
		return "No lower-right corner defined!\n"
	return ""
