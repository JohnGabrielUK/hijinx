extends Area2D

const obj_conversation = preload("res://objects/ui/Conversation.tscn")

export (String) var conversation_slug
export (NodePath) var path_gamemaster

onready var gamemaster = get_node(path_gamemaster)

signal conversation_finished

func interact():
	# Get the camera to center on us
	var players = get_tree().get_nodes_in_group("player")
	if players.size() != 0:
		var player = players[0]
		var corner = global_position - Vector2(213, 120)
		player.set_camera_bounds(9999, corner, corner + Vector2(426, 240))
	# Tell the gamemaster to pause the timer
	gamemaster.pause_timer()
	var conversation = obj_conversation.instance()
	conversation.conversation_slug = conversation_slug
	conversation.connect("done", self, "done")
	add_child(conversation)

func done():
	gamemaster.unpause()
	emit_signal("conversation_finished")
	# Get the camera to center on us
	var players = get_tree().get_nodes_in_group("player")
	if players.size() != 0:
		var player = players[0]
		player.unset_camera_bounds(9999)