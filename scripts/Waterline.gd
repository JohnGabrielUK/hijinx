tool
extends Polygon2D

const obj_splash = preload("res://objects/effects/Splash.tscn")
const material_simple = preload("res://shaders/water_simple.tres")
const material_shader = preload("res://shaders/water_shader.tres")

onready var collider = $Area2D/CollisionPolygon2D

var waterline

func set_shader(on : bool) -> void:
	if on:
		material = material_shader
	else:
		material = material_simple

func _next_frame():
	pass

func _body_entered(body):
	if body.is_in_group("player"):
		body.enter_water()
		var splash = obj_splash.instance()
		add_child(splash)
		splash.global_position = Vector2(body.global_position.x, waterline)

func _body_exited(body):
	if body.is_in_group("player"):
		body.exit_water()

func _ready():
	set_shader(Options.get_tickbox_value("water_shader"))
	collider.polygon = self.polygon
	waterline = 9001
	for next in polygon:
		if next.y + global_position.y < waterline:
			waterline = next.y + global_position.y