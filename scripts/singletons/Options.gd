extends Node

const SAVE_PATH = "user://settings.cfg"

const actions = {
	"up": "Up",
	"down": "Down",
	"left": "Left",
	"right": "Right",
	"jump": "Jump",
	"interact": "Interact",
	"pause": "Pause",
	"restart": "Restart"
}

const tickbox_defaults = {
	"fullscreen": false,
	"complex_lights": true,
	"flame_shader": true,
	"water_shader": true,
	"lava_shader": true,
	"show_tips": true
}

var config

func update_lights():
	for next_light in get_tree().get_nodes_in_group("light"):
		next_light.visible = get_complex_lights()

func update_shaders() -> void:
	var use_flame_shader = get_tickbox_value("flame_shader")
	var use_water_shader = get_tickbox_value("water_shader")
	var use_lava_shader = get_tickbox_value("lava_shader")
	for next_water in get_tree().get_nodes_in_group("water_shader"):
		next_water.set_shader(use_water_shader)
	for next_flame in get_tree().get_nodes_in_group("flame"):
		next_flame.set_shader(use_flame_shader)
	for next_lava in get_tree().get_nodes_in_group("lava"):
		next_lava.set_shader(use_lava_shader)

func update_volumes():
	for next in ["BGM", "SFX", "AMB", "UI"]:
		var index = AudioServer.get_bus_index(next)
		var volume = get_channel_volume(next)
		AudioServer.set_bus_volume_db(index, linear2db(volume))
	
func update_controls():
	for next_action in actions:
		var key = get_key_binding(next_action)
		var button = get_gamepad_binding(next_action)
		bind_action(next_action, key, button)

func bind_action(action, key, button):
	var event_key = InputEventKey.new()
	event_key.scancode = key
	var event_button = InputEventJoypadButton.new()
	event_button.button_index = button
	# Remove any old bindings
	InputMap.action_erase_events(action)
	# Add the new ones
	InputMap.action_add_event(action, event_key)
	InputMap.action_add_event(action, event_button)

func get_channel_volume(channel):
	if channel == "BGM":
		return config.get_value("volume", channel, 0.5)
	return config.get_value("volume", channel, 1.0)

func get_complex_lights():
	return config.get_value("settings", "complex_lights", true)

func get_tickbox_value(tickbox):
	return config.get_value("settings", tickbox, tickbox_defaults[tickbox])

func get_key_binding(action):
	var default = InputMap.get_action_list(action)[0].scancode
	return config.get_value("keybindings", action, default)

func get_gamepad_binding(action):
	var default = InputMap.get_action_list(action)[1].button_index
	return config.get_value("gamepad_bindings", action, default)

func set_channel_volume(channel, value):
	config.set_value("volume", channel, value)
	update_volumes()

func set_complex_lights(value):
	config.set_value("settings", "complex_lights", value)
	update_lights()

func set_tickbox_value(tickbox, value):
	config.set_value("settings", tickbox, value)

func set_key_binding(action, key):
	config.set_value("keybindings", action, key)
	update_controls()

func set_gamepad_binding(action, button):
	config.set_value("gamepad_bindings", action, button)
	update_controls()

func reset_bindings():
	InputMap.load_from_globals()
	for current_action in actions:
		var key = InputMap.get_action_list(current_action)[0].scancode
		var button = InputMap.get_action_list(current_action)[1].button_index
		config.set_value("keybindings", current_action, key)
		config.set_value("gamepad_bindings", current_action, button)

func apply_tickbox_settings():
	var fullscreen = get_tickbox_value("fullscreen")
	OS.window_fullscreen = fullscreen
	set_complex_lights(get_tickbox_value("complex_lights"))
	update_lights()
	update_shaders()

func save_settings():
	config.save(SAVE_PATH)

func _enter_tree():
	config = ConfigFile.new()
	config.load(SAVE_PATH)
	apply_tickbox_settings()
	update_volumes()
	update_controls()
