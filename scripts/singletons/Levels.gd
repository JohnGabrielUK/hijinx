extends Node

var levels = {
	"Tutorial": {
		"name": "Proving Grounds",
		"curses": null,
		"scene": "res://levels/Tutorial.tscn",
		"next_level": null,
		"music": "res://music/Inspired.ogg",
		"speedrunnable": false
	},
	"A": {
		"name": "Coins and Columns",
		"curses": {
			Curses.CANNOT_LAND: {"par": 11000, "devs_best": 9200},
			Curses.GRAVITY_FLIPPING: {"par": 7000, "devs_best": 5250},
			Curses.TIME_LIMIT: {"par": 7000, "devs_best": 5200},
			Curses.GREED: {"par": 32000, "devs_best": 28500}
		},
		"scene": "res://levels/LevelA.tscn",
		"next_level": "B",
		"music": "res://music/Fantasia Fantasia.ogg",
		"speedrunnable": true
	},
	"B": {
		"name": "Underwater Labyrinth",
		"curses": {
			Curses.TIME_LIMIT: {"par": 37000, "devs_best": 32700},
			Curses.GREED: {"par": 100000, "devs_best": 90874}
		},
		"scene": "res://levels/LevelB.tscn",
		"next_level": "C",
		"music": "res://music/Miami Viceroy.ogg",
		"speedrunnable": true
	},
	"C": {
		"name": "Spiky Chute",
		"curses": {
			Curses.WEAK: {"par": 15000, "devs_best": 10000},
			Curses.CANNOT_LAND: {"par": 15000, "devs_best": 12100},
			Curses.TIME_LIMIT: {"par": 15000, "devs_best": 11300},
			Curses.GREED: {"par": 40000, "devs_best": 33200}
		},
		"scene": "res://levels/LevelC.tscn",
		"next_level": "D",
		"music": "res://music/Voltaic.ogg",
		"speedrunnable": true
	},
	"D": {
		"name": "Hall of Many Doors",
		"curses": {
			Curses.TIME_LIMIT: {"par": 50000, "devs_best": -1},
			Curses.GRAVITY_FLIPPING: {"par": 20000, "devs_best": 15300},
			Curses.GREED: {"par": 90000, "devs_best": 78800}
		},
		"scene": "res://levels/LevelD.tscn",
		"next_level": "E",
		"music": "res://music/Werq.ogg",
		"speedrunnable": true
	},
	"E": {
		"name": "The Stronghold",
		"curses": {
			Curses.WEAK: {"par": 60000, "devs_best": 50500},
			Curses.TIME_LIMIT: {"par": 75000, "devs_best": 51200},
			Curses.GREED: {"par": 100000, "devs_best": 88000}
		},
		"scene": "res://levels/LevelE.tscn",
		"next_level": "F",
		"music": "res://music/Exit the Premises.ogg",
		"speedrunnable": true
	},
	"F": {
		"name": "The Fiery Tide",
		"curses": {
			Curses.WEAK: {"par": 52000, "devs_best": 47400},
			Curses.TIME_LIMIT: {"par": 52000, "devs_best": 47700},
			Curses.GRAVITY_FLIPPING: {"par": 85000, "devs_best": 79700},
			Curses.GREED: {"par": 90000, "devs_best": 82600}
		},
		"scene": "res://levels/LevelF.tscn",
		"next_level": "G",
		"music": "res://music/Exit the Premises.ogg",
		"speedrunnable": true
	},
	"G": {
		"name": "Easy Come, Easy Go",
		"curses": {
			Curses.WEAK: {"par": 15000, "devs_best": 12900},
			Curses.TIME_LIMIT: {"par": 15000, "devs_best": 13000},
			Curses.GREED: {"par": 60000, "devs_best": 54400}
		},
		"scene": "res://levels/LevelG.tscn",
		"next_level": "H",
		"music": "res://music/Fantasia Fantasia.ogg",
		"speedrunnable": true
	},
	"H": {
		"name": "Newton's Bane",
		"curses": {
			Curses.TIME_LIMIT: {"par": 45000, "devs_best": 38000},
			Curses.GREED: {"par": 60000, "devs_best": 48900}
		},
		"scene": "res://levels/LevelH.tscn",
		"next_level": "I",
		"music": "res://music/Voltaic.ogg",
		"speedrunnable": true
	},
	"I": {
		"name": "The Anti-Promenade",
		"curses": {
			Curses.TIME_LIMIT: {"par": 30000, "devs_best": 23600},
			Curses.GREED: {"par": 70000, "devs_best": 58000},
			Curses.GRAVITY_FLIPPING: {"par": 45000, "devs_best": 40100},
			Curses.CANNOT_LAND: {"par": -1, "devs_best": -1}
		},
		"scene": "res://levels/LevelI.tscn",
		"next_level": null,
		"music": "res://music/Werq.ogg",
		"speedrunnable": true
	},
	"Secret": {
		"name": "Winged's Lair",
		"curses": {
			Curses.JERK: {"par": -1, "devs_best": -1}
		},
		"scene": "res://levels/Secret.tscn",
		"next_level": null,
		"music": "res://music/Future Gladiator.ogg",
		"speedrunnable": false
	},
	"Ending": {
		"name": "The Final Decision",
		"par": 50000,
		"curses": null,
		"scene": "res://levels/Ending.tscn",
		"next_level": null,
		"music": null,
		"speedrunnable": false
	}
}

func level_has_curses(which):
	return levels[which]["curses"] != null

func level_has_music(which):
	return levels[which]["music"] != null

func get_level_name(which):
	return levels[which]["name"]

func get_level_curse_par(which_level, which_curse):
	return levels[which_level]["curses"][which_curse]["par"]

func get_devs_best_for_curse(which_level, which_curse):
	return levels[which_level]["curses"][which_curse]["devs_best"]

func get_level_curses(which):
	return levels[which]["curses"]

func get_level_scene(which):
	return levels[which]["scene"]

func get_level_music(which):
	return levels[which]["music"]

func get_level_next(which):
	return levels[which]["next_level"]

func is_level_speedrunnable(which) -> bool:
	return levels[which]["speedrunnable"]

func does_curse_count(level, curse) -> bool:
	# Yeah, it's a hack. I don't feel like refactoring the whole thing.
	if curse == Curses.JERK: return false
	if level == "I" and curse == Curses.CANNOT_LAND:
		return false
	return true
