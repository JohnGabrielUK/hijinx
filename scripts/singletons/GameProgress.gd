extends Node

const SAVE_PATH = "user://progress.json"
const GHOST_PATH = "user://ghosts"
const STARTING_POS = Vector2(88, 168)
const TOTAL_TIPS = 3

var level_progress : Dictionary
var map_position : Vector2
var new_game : bool
var tips_shown : int
var game_finished : bool
var speedrun_segments : Dictionary
var speedrun_complete : bool

func ghost_run_exists(level : String, curse : int) -> bool:
	var f = File.new()
	return f.file_exists(GHOST_PATH + "/" + level + str(curse) + ".ghost")

func save_ghost_run(level : String, curse : int, time : int, ghost_recording : Array) -> void:
	if ghost_run_exists(level, curse) and time > get_best_time_for_curse(level, Curses.get_curse_slug(curse)): return
	var f = File.new()
	var err = f.open(GHOST_PATH + "/" + level + str(curse) + ".ghost", File.WRITE)
	f.store_16(ghost_recording.size())
	for current_frame in ghost_recording:
		f.store_float(current_frame["position"].x)
		f.store_float(current_frame["position"].y)
		f.store_8(int(current_frame["flip_h"]))
		f.store_8(int(current_frame["flip_v"]))
		f.store_8(current_frame["animation"])
		f.store_8(current_frame["frame"])
	# Write each frame of the movements
	f.close()

func load_ghost_run(level : String, curse : int) -> Array:
	var f = File.new()
	var err = f.open(GHOST_PATH + "/" + level + str(curse) + ".ghost", File.READ)
	var length = f.get_16()
	var results = []
	for i in range(0, length):
		var frame = {
			"position": Vector2(f.get_float(), f.get_float()),
			"flip_h": bool(f.get_8()),
			"flip_v": bool(f.get_8()),
			"animation": f.get_8(),
			"frame": f.get_8()
		}
		results.append(frame)
	f.close()
	return results

func get_level_best_time(level : String) -> int:
	return level_progress[level]["curses"]["time_limit"]["best_time"]

func get_best_time_for_curse(level : String, curse : String) -> int:
	return level_progress[level]["curses"][curse]["best_time"]

func get_total_curses():
	var result = 0
	for current_level in level_progress:
		for current_curse in get_curses_for_level(current_level):
			if Levels.does_curse_count(current_level, Curses.get_curse_by_slug(current_curse)):
				result += 1
	return result

func get_curses_for_level(level):
	return level_progress[level]["curses"]

func is_curse_cleared(level, curse):
	if not level_progress.has(level): return false
	return level_progress[level]["curses"][curse]["cleared"]

func init_level_curse(level, curse):
	var curse_slug = Curses.get_curse_slug(curse)
	level_progress[level]["curses"][curse_slug] = {
		"cleared": false, "best_time": -1
	}

func set_curse_cleared(level, curse, value, clear_time):
	var curse_slug = Curses.get_curse_slug(curse)
	var curse_data = level_progress[level]["curses"][curse_slug]
	curse_data["cleared"] = value
	if curse_data["best_time"] == -1 or clear_time < curse_data["best_time"]:
		curse_data["best_time"] = clear_time
	level_progress[level]["curses"][curse_slug] = curse_data

func get_curses_cleared_for_level(level):
	var result = 0
	for current_curse in level_progress[level]["curses"]:
		if level_progress[level]["curses"][current_curse]["cleared"] == true:
			result += 1
	return result

func get_curses_cleared():
	var result = 0
	for current_level in level_progress:
		for current_curse in get_curses_for_level(current_level):
			if is_curse_cleared(current_level, current_curse):
				if Levels.does_curse_count(current_level, Curses.get_curse_by_slug(current_curse)):
					result += 1
	return result

func get_speedrun_total_time():
	var result = 0
	for current_level in level_progress:
		if Levels.is_level_speedrunnable(current_level):
			var level_time = speedrun_segments[current_level]
			# If any section of the speedrun doesn't have a time, abort
			if level_time == -1: return -1
			result += level_time
	return result

func get_speedrun_segment(level):
	return speedrun_segments[level]

func set_speedrun_segment(level, time):
	speedrun_segments[level] = time

func is_speedrun_complete():
	return speedrun_complete

func is_level_completed(level):
	if not Levels.level_has_curses(level):
		return false
	return get_curses_cleared_for_level(level) == get_curses_for_level(level).size()

func new_game():
	new_game = true
	map_position = STARTING_POS
	level_progress = {}
	game_finished = false
	speedrun_segments = {}
	speedrun_complete = false
	for current_level in Levels.levels:
		var curses = Levels.levels[current_level]["curses"]
		# Some levels have no curses, and therefore do not count towards progression
		if curses != null:
			level_progress[current_level] = {"unlocked": false, "curses": {}}
			for current_curse in curses:
				# Mark this curse as not having been cleared yet
				init_level_curse(current_level, current_curse)
		# Add section in speedrun record
		speedrun_segments[current_level] = -1
	save_game()
	# Make the directory for keeping ghost runs
	var d = Directory.new()
	d.make_dir(GHOST_PATH)

func load_game():
	var f = File.new()
	var err = f.open(SAVE_PATH, File.READ)
	var contents = f.get_as_text()
	var parsed : Dictionary = parse_json(contents)
	level_progress = parsed["levels"]
	map_position = Vector2(parsed["map_position"]["x"], parsed["map_position"]["y"])
	new_game = parsed["new_game"]
	# Let's check to see if this is a save from v1.0 - if it is, we need to add some new variables
	tips_shown = parsed["tips_shown"] if parsed.has("tips_shown") else 0
	if parsed.has("game_finished"):
		game_finished = parsed["game_finished"]
	else:
		game_finished = true if get_curses_cleared() >= get_total_curses() else false
	if parsed.has("speedrun"):
		speedrun_segments = parsed["speedrun"]["segments"]
		speedrun_complete = parsed["speedrun"]["complete"]
	else:
		speedrun_segments = {}
		for current_level in Levels.levels:
			# Add section in speedrun record
			if Levels.is_level_speedrunnable(current_level):
				speedrun_segments[current_level] = -1
		speedrun_complete = false
	f.close()
	# Make the directory for keeping ghost runs
	var d = Directory.new()
	if not d.dir_exists(GHOST_PATH):
		d.make_dir(GHOST_PATH)

func save_game():
	var f = File.new()
	var err = f.open(SAVE_PATH, File.WRITE)
	var to_write = {
		"new_game": new_game,
		"map_position": {
			"x": map_position.x,
			"y": map_position.y
		},
		"levels": level_progress,
		"tips_shown": tips_shown,
		"game_finished": game_finished,
		"speedrun": {
			"segments": speedrun_segments,
			"complete": speedrun_complete
		}
	}
	var json = to_json(to_write)
	f.store_line(json)
	f.close()

func save_exists():
	var f = File.new()
	return f.file_exists(SAVE_PATH)

func _ready():
	if save_exists():
		load_game()
	else:
		new_game()