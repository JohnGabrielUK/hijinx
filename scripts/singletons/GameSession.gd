extends Node

enum GAME_MODE { NORMAL, SPEEDRUN }

onready var current_mode : int = GAME_MODE.NORMAL

var speedrun_start_time
var speedrun_pause_time
var speedrun_time_spent_paused
var speedrun_time # Only to be used when the speedrun is complete
var segments = {}
onready var speedrun_started = false

func is_speedrun():
	return current_mode == GAME_MODE.SPEEDRUN

func speedrun_start() -> void:
	# Make sure the speedrun hasn't already started
	if speedrun_started:
		speedrun_resume_timer()
		return
	# Okay, start the timer!
	speedrun_start_time = OS.get_ticks_msec()
	speedrun_time_spent_paused = 0
	speedrun_started = true

func set_level_finished(level : String, time : int) -> void:
	segments[level] = time

func speedrun_finish() -> void:
	speedrun_time = speedrun_get_time_elapsed()
	if speedrun_time < GameProgress.get_speedrun_total_time() or GameProgress.speedrun_complete == false:
		GameProgress.speedrun_complete = true
		var total = 0
		for level in segments:
			GameProgress.set_speedrun_segment(level, segments[level])
			total += segments[level]
		GameProgress.save_game()

func speedrun_pause_timer() -> void:
	speedrun_pause_time = OS.get_ticks_msec()

func speedrun_resume_timer() -> void:
	speedrun_time_spent_paused += OS.get_ticks_msec() - speedrun_pause_time

func speedrun_get_time_elapsed() -> int:
	return OS.get_ticks_msec() - speedrun_start_time - speedrun_time_spent_paused
