extends Node

const CANNOT_JUMP = 0
const CANNOT_LAND = 1
const CANNOT_STOP = 2
const CANNOT_WALLGRIP = 3
const GRAVITY_FLIPPING = 4
const TIME_LIMIT = 5
const GREED = 6
const WEAK = 7
const JERK = 8

var curses = {
	CANNOT_JUMP: {
		"slug": "cannot_jump",
		"name": "Grounded",
		"description": "Cannot jump.",
		"card_index": 6
	},
	CANNOT_LAND: {
		"slug": "cannot_land",
		"name": "The Floor is Lava",
		"description": "Landing is fatal.",
		"card_index": 3
	},
	CANNOT_STOP: {
		"slug": "cannot_stop",
		"name": "Gotta Go Fast",
		"description": "Being stationary is fatal.",
		"card_index": 6
	},
	GRAVITY_FLIPPING: {
		"slug": "gravity_flip",
		"name": "Falling Upwards",
		"description": "Gravity intermittently changes direction.",
		"card_index": 1
	},
	TIME_LIMIT: {
		"slug": "time_limit",
		"name": "Speedrunners' Complex",
		"description": "Ridiculous time limit imposed.",
		"card_index": 4
	},
	CANNOT_WALLGRIP: {
		"slug": "cannot_wallgrip",
		"name": "Slippery",
		"description": "Cannot grip walls.",
		"image": preload("res://sprites/cards/card_cannotclimb.png"),
		"card_index": 6
	},
	GREED: {
		"slug": "greed",
		"name": "Completionism",
		"description": "Must collect every coin.",
		"image": preload("res://sprites/cards/card_greed.png"),
		"card_index": 2
	},
	WEAK: {
		"slug": "weak",
		"name": "One-Hit Wonder",
		"description": "All damage is fatal.",
		"image": preload("res://sprites/cards/card_weak.png"),
		"card_index": 5
	},
	JERK: {
		"slug": "jerk",
		"name": "Jerk",
		"description": "Good luck.",
		"image": preload("res://sprites/cards/card_weak.png"),
		"card_index": 7
	}
}

var current_curse

func get_curse_by_slug(slug):
	for current_curse in curses:
		if curses[current_curse]["slug"] == slug:
			return current_curse
	return null

func get_curse_slug(which):
	return curses[which]["slug"]

func get_curse_name(which):
	return curses[which]["name"]

func get_curse_description(which):
	return curses[which]["description"]

func get_curse_image_index(which):
	return curses[which]["card_index"]

func set_current_curse(which):
	current_curse = which

func get_current_curse() -> int:
	return current_curse
