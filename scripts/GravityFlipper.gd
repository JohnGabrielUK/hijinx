extends Area2D

const ANIM_SPEED = 6

onready var sprite = $Sprite
onready var audio_gravity_up = $Audio_GravityUp
onready var audio_gravity_down = $Audio_GravityDown

onready var anim_index = 0

func _process(delta):
	anim_index += delta
	sprite.frame = int(anim_index * ANIM_SPEED) % sprite.hframes

func _body_entered(body):
	if body is Player:
		body.change_gravity(Vector2(0, scale.y * -1))
		if scale.y > 0:
			audio_gravity_up.play()
		else:
			audio_gravity_down.play()
