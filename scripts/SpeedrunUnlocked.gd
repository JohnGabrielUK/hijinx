extends Control

onready var label = $Label
onready var label2 = $Label2
onready var label3 = $Label3
onready var audio = $AudioStreamPlayer
onready var tween = $Tween

func _ready():
	for current_label in [label, label2, label3]:
		current_label.modulate = Color.black
	tween.interpolate_property(label, "modulate", Color.black, Color.white, 0.25, Tween.TRANS_LINEAR, Tween.EASE_OUT, 0.0)
	tween.interpolate_property(label2, "modulate", Color.black, Color.white, 0.25, Tween.TRANS_LINEAR, Tween.EASE_OUT, 1.0)
	tween.interpolate_property(label3, "modulate", Color.black, Color.white, 0.25, Tween.TRANS_LINEAR, Tween.EASE_OUT, 2.5)
	tween.interpolate_property(label, "modulate", Color.white, Color.black, 0.5, Tween.TRANS_LINEAR, Tween.EASE_OUT, 5.0)
	tween.interpolate_property(label2, "modulate", Color.white, Color.black, 0.5, Tween.TRANS_LINEAR, Tween.EASE_OUT, 5.0)
	tween.interpolate_property(label3, "modulate", Color.white, Color.black, 0.5, Tween.TRANS_LINEAR, Tween.EASE_OUT, 5.0)
	yield(get_tree().create_timer(2.0), "timeout")
	tween.start()
	audio.play()
	yield(tween, "tween_all_completed")
	get_tree().change_scene("res://scenes/TitleScreen.tscn")
