extends KinematicBody2D

onready var sprite = $Sprite
onready var collider = $CollisionShape2D
onready var raycasts = [$RayCast2D_Left, $RayCast2D_Center, $RayCast2D_Right]
onready var timer = $Timer
onready var timer_respawn = $Timer_Respawn
onready var audio_click = $Audio_Click
onready var tween = $Tween

const STATE_NORMAL = 0
const STATE_ABOUT_TO_FALL = 1
const STATE_FALLING = 2

const MAX_FALL_SPEED = 192
const FALL_INCR = 386

onready var init_position = global_position

var state = STATE_NORMAL
var fall_speed = 0

func colliding():
	for next in raycasts:
		if next.is_colliding():
			return true
	return false

func _process(delta):
	if state == STATE_NORMAL:
		if colliding():
			state = STATE_ABOUT_TO_FALL
			audio_click.play()
			timer.start()
	elif state == STATE_ABOUT_TO_FALL:
		sprite.offset.x = randf() * 2
	elif state == STATE_FALLING:
		fall_speed = clamp(fall_speed, fall_speed + (FALL_INCR * delta), MAX_FALL_SPEED)
		position.y += fall_speed * delta

func _fall():
	collider.disabled = true
	state = STATE_FALLING
	timer_respawn.start()

func reset():
	global_position = init_position
	fall_speed = 0
	collider.disabled = false
	state = STATE_NORMAL
	timer.stop()
	timer_respawn.stop()

func _on_Timer_Respawn_timeout():
	modulate = Color(1.0, 1.0, 1.0, 0.0)
	reset()
	tween.interpolate_property(self, "modulate", Color(0.0, 0.0, 0.0, 0.0), Color.white, 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
