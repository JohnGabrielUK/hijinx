extends Node2D

const MOVE_SPEED = 150

export (NodePath) var path_route

onready var route = get_node(path_route)

onready var moving = false
onready var target_position = position

onready var sprite = $Sprite
onready var audio_step = $Audio_Step

func get_current_node():
	for current_node in get_tree().get_nodes_in_group("map_node"):
		if current_node.global_position == global_position:
			return current_node
	return null

func try_to_move(destination : Vector2):
	# Flip sprite for direction
	match destination:
		Vector2.LEFT:
			sprite.flip_h = true
		Vector2.RIGHT:
			sprite.flip_h = false
	target_position = position
	# First of all, are we on a node that blocks forward progress?
	var current_node = get_current_node()
	if current_node != null:
		match destination:
			Vector2.LEFT:
				if current_node.block_left: return
			Vector2.RIGHT:
				if current_node.block_right: return
			Vector2.UP:
				if current_node.block_up: return
			Vector2.DOWN:
				if current_node.block_down: return
	# Okay, we can move freely! Carry merrily on
	var done = false
	while !done:
		var map_pos = route.world_to_map(target_position + (destination*16))
		if route.get_cell(map_pos.x, map_pos.y) != -1:
			target_position += destination*32
			done = true
			moving = true
			audio_step.play()
		else:
			done = true

func _input(event):
	if moving: return
	if event.is_action_pressed("left"):
		try_to_move(Vector2.LEFT)
	if event.is_action_pressed("right"):
		try_to_move(Vector2.RIGHT)
	if event.is_action_pressed("up"):
		try_to_move(Vector2.UP)
	if event.is_action_pressed("down"):
		try_to_move(Vector2.DOWN)

func _process(delta):
	if moving:
		var change = MOVE_SPEED * delta
		if position.x > target_position.x:
			if position.x > target_position.x + change:
				position.x -= change
			else:
				position = target_position
		elif position.x < target_position.x:
			if position.x < target_position.x - change:
				position.x += change
			else:
				position = target_position
		if position.y > target_position.y:
			if position.y > target_position.y + change:
				position.y -= change
			else:
				position = target_position
		elif position.y < target_position.y:
			if position.y < target_position.y - change:
				position.y += change
			else:
				position = target_position
		# Are we done moving?
		if position == target_position:
			moving = false