extends Node2D

export (String) var level_slug
export (bool) var block_up = false
export (bool) var block_down = false
export (bool) var block_left = false
export (bool) var block_right = false

onready var sprite = $Sprite

func _ready():
	if Levels.level_has_curses(level_slug):
		# If we've cleared at least one curse on this course, we can keep going
		if GameProgress.get_curses_cleared_for_level(level_slug) > 0:
			block_up = false
			block_down = false
			block_left = false
			block_right = false
	if GameProgress.is_level_completed(level_slug) or level_slug == "Tutorial": # hacky hack
		sprite.frame = 2
	elif level_slug != "Ending" and GameProgress.get_curses_cleared_for_level(level_slug) > 0:
		sprite.frame = 1
