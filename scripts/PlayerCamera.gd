extends Camera2D

const ZOOM_NORMAL = 0.333
const MOVE_SPEED = 400 #192
const CAMERA_SIZE = Vector2(426, 240)

var target_position : Vector2 = Vector2(0, 0)
var limit : Rect2 = Rect2(Vector2(0, 0), Vector2(4096, 4096))

onready var zoom_target = ZOOM_NORMAL
onready var zoom_actual = ZOOM_NORMAL

func set_zoom_target(amount):
	zoom_target = amount

func set_zoom_actual(amount):
	zoom_actual = amount
	zoom_target = amount

func reset_zoom():
	zoom.x = ZOOM_NORMAL
	zoom.y = ZOOM_NORMAL

func _process(delta):
	var target_topleft = target_position - (CAMERA_SIZE/2)
	var move_amount = MOVE_SPEED * delta
	var move_range := Rect2(global_position - (Vector2.ONE * move_amount), Vector2.ONE * move_amount * 2)
	target_topleft.x = clamp(target_topleft.x, limit.position.x, limit.end.x - CAMERA_SIZE.x)
	target_topleft.y = clamp(target_topleft.y, limit.position.y, limit.end.y - CAMERA_SIZE.y)
	global_position.x = clamp(target_topleft.x, move_range.position.x, move_range.end.x)
	global_position.y = clamp(target_topleft.y, move_range.position.y, move_range.end.y)
