extends Polygon2D

const material_shader = preload("res://shaders/lava_shader.tres")
const material_simple = preload("res://shaders/lava_simple.tres")

onready var collider = $Area2D/CollisionPolygon2D
onready var glow = $Glow
onready var audio_bubbling = $Audio_Bubbling
onready var audio_move = $Audio_Move

func play_move_audio() -> void:
	audio_move.play()

func position_audio() -> void:
	var camera_nodes = get_tree().get_nodes_in_group("camera")
	if camera_nodes.size() > 0:
		var camera = camera_nodes[0]
		for current_audio in [audio_bubbling, audio_move]:
			current_audio.global_position.x = camera.global_position.x + (camera.CAMERA_SIZE.x / 2.0)

func _physics_process(delta : float) -> void:
	position_audio()

func set_shader(on : bool) -> void:
	if on:
		material = material_shader
	else:
		material = material_simple

func _ready() -> void:
	set_shader(Options.get_tickbox_value("lava_shader"))
	collider.polygon = self.polygon
	var max_width = 0
	for current_point in polygon:
		max_width = max(max_width, current_point.x)
	glow.polygon[2].x = max_width
	glow.polygon[3].x = max_width

func _enter_tree() -> void:
	position_audio()