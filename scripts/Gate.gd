extends StaticBody2D

const ANIM_SPEED = 15

onready var sprite = $Sprite
onready var indicator = $Indicator
onready var warning = $Warning
onready var collider = $CollisionShape2D
onready var occluder = $LightOccluder2D
onready var audio = $Audio_Toggle

var closed = true

onready var animation_index = 0
onready var animation_target = 0

export (int) var indicator_index

func _process(delta):
	if animation_index > animation_target + (delta * ANIM_SPEED):
		animation_index -= delta * ANIM_SPEED
	elif animation_index < animation_target - (delta * ANIM_SPEED):
		animation_index += delta * ANIM_SPEED
	sprite.frame = animation_index

func open():
	collider.disabled = true
	occluder.hide()
	closed = false
	animation_target = 6
	if find_node("Warning"):
		warning.hide()

func close():
	collider.disabled = false
	occluder.show()
	closed = true
	animation_target = 0
	if find_node("Warning") and Curses.current_curse == Curses.CANNOT_LAND:
		warning.show()

func toggle():
	audio.play()
	if closed: open()
	else: close()

func reset():
	close()

func _ready():
	indicator.frame = indicator_index
	if rotation_degrees == 0:
		warning.queue_free()
	elif rotation_degrees == -90:
		warning.flip_h = false