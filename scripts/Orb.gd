extends Area2D

onready var sprite = $Sprite

onready var anim_index = 0

signal reached

func _process(delta):
	anim_index += delta
	var offset = sin(deg2rad(anim_index * 100)) * 3
	sprite.offset.y = offset
	sprite.frame = int(anim_index * 10) % 10

func _body_entered(body):
	if body.is_in_group("player"):
		emit_signal("reached")
