extends StaticBody2D

onready var timer = $Timer
onready var collider = $CollisionShape2D
onready var sprite = $Sprite
onready var raycasts = [$RayCast2D_Center, $RayCast2D_Left, $RayCast2D_Right]
onready var audio_click = $Audio_Triggered
onready var audio_open = $Audio_Opening
onready var audio_close = $Audio_Closing

const ANIM_SPEED = 15

const STATE_CLOSED = 0
const STATE_OPENING = 1
const STATE_OPEN = 2

onready var animation_index = 0
onready var animation_target = 0

onready var state = STATE_CLOSED

func is_colliding():
	for next in raycasts:
		if next.is_colliding(): return true
	return false

func _process(delta):
	if state == STATE_CLOSED and is_colliding():
		audio_click.play()
		state = STATE_OPENING
		timer.wait_time = 1
		timer.start()
	# Sprite animation
	if animation_index > animation_target + (delta * ANIM_SPEED):
		animation_index -= delta * ANIM_SPEED
	elif animation_index < animation_target - (delta * ANIM_SPEED):
		animation_index += delta * ANIM_SPEED
	sprite.frame = animation_index

func _on_Timer_timeout():
	if state == STATE_OPENING:
		animation_target = 4
		audio_open.play()
		collider.disabled = true
		state = STATE_OPEN
		timer.wait_time = 3
		timer.start()
	elif state == STATE_OPEN:
		animation_target = 0
		audio_close.play()
		collider.disabled = false
		state = STATE_CLOSED

func reset():
	timer.stop()
	state = STATE_CLOSED
	animation_index = 0
	animation_target = 0
	collider.disabled = false
