extends Sprite

func _process(delta):
	global_position.y -= 8 * delta * scale.y

func _on_Timer_timeout():
	if frame < hframes - 1:
		frame += 1
	else:
		queue_free()
