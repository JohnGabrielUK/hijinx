extends AnimatedSprite

func _ready():
	play("default")

func _animation_finished():
	hide()

func _audio_finished():
	queue_free()
