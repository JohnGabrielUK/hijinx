extends Sprite

func _process(delta):
	if flip_h:
		global_position.x += 8 * delta
	else:
		global_position.x -= 8 * delta

func _on_Timer_timeout():
	if frame < hframes - 1:
		frame += 1
	else:
		queue_free()
