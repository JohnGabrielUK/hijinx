extends Sprite

onready var timer = $Timer

func _ready():
	timer.wait_time = 0.05 + (randf()*0.1)

func _process(delta):
	global_position.y -= 8 * delta
	if flip_h:
		global_position.x -= 8 * delta
	else:
		global_position.x += 8 * delta

func _on_Timer_timeout():
	if frame < hframes - 1:
		frame += 1
	else:
		queue_free()
