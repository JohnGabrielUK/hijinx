extends Node2D

onready var back_buffer = $BackBufferCopy
onready var sprite_static = $Sprite_Static

func set_shader(on : bool) -> void:
	if on:
		back_buffer.show()
		sprite_static.hide()
	else:
		back_buffer.hide()
		sprite_static.show()

func _ready() -> void:
	set_shader(Options.get_tickbox_value("flame_shader"))
