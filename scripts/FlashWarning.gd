extends Node2D

onready var flash_index = 0.0

func _physics_process(delta : float) -> void:
	flash_index += delta
	modulate = Color(1.0, 1.0, 1.0, abs(sin(flash_index)))
