extends Control

const FRAME_SIZE = 32

const obj_menuitem = preload("res://objects/ui/components/MenuItem.tscn")

onready var label_dialogue = $Frame/VBox_Text/Label_Dialogue
onready var texture_mugshot = $Frame/TextureRect_Mugshot
onready var menu_answers = $Frame/VBox_Text/HBox/Menu_Answers
onready var timer = $Timer
onready var anim_player = $AnimationPlayer
onready var audio_voicewriter = $Audio_Voicewriter

var lines
var answers
var is_question

onready var current_line = 0
onready var showing_answers = false

signal done
signal answer_selected
signal done_disappearing

func set_mugshot(speaker, frame):
	var mugshot_filename = "res://sprites/convo/%s.png" % [speaker]
	var mugshot = load(mugshot_filename)
	var atlas = AtlasTexture.new()
	atlas.atlas = mugshot
	atlas.region = Rect2(frame * FRAME_SIZE, 0, FRAME_SIZE, FRAME_SIZE)
	texture_mugshot.texture = atlas

func display_line():
	var line : Dictionary = lines[current_line]
	audio_voicewriter.stream = load("res://sounds/voicewriters/" + line["speaker"] + ".wav")
	set_mugshot(line["speaker"], line["frame"])
	label_dialogue.text = line["dialogue"]
	label_dialogue.visible_characters = 0
	timer.start()
#	if line.has("vo"):
#		var vo_filename = "res://sounds/voice/" + line["vo"]
#		audio_voiceover.stream = load(vo_filename)
#		audio_voiceover.call_deferred("play")

func _type():
	var letters_to_type = 1
	if Input.is_action_pressed("interact"):
		letters_to_type = 5
	for i in range(0, letters_to_type):
		label_dialogue.visible_characters += 1
		if label_dialogue.percent_visible == 1.00:
			timer.stop()
	audio_voicewriter.play()

func _unhandled_input(event):
	if event is InputEvent:
		if event.is_action_pressed("jump"):
			# Don't worry, Mr. Tree! We'll handle this.
			get_tree().set_input_as_handled()
			if label_dialogue.percent_visible >= 1.00:
				next_line()

func next_line():
	current_line += 1
	if current_line >= lines.size():
		done()
	else:
		display_line()

#func answer_selected(next_section):
#	# We're just passing the message on
#	emit_signal("answer_selected", next_section)

func init_answers():
	for next in answers:
		var answer = obj_menuitem.instance()
		menu_answers.add_child(answer)
		answer.set_text(next)
		#answer.section = answers[next]
		#answer.connect("selected", self, "answer_selected")
	menu_answers._ready()
	showing_answers = true
	menu_answers.active = true

func done():
	if is_question:
		if not showing_answers:
			init_answers()
	else:
		emit_signal("done")

func appear():
	anim_player.play("appear")
	yield(anim_player, "animation_finished")

func disappear():
	anim_player.play("disappear")
	yield(anim_player, "animation_finished")
	emit_signal("done_disappearing")

func _ready():
	display_line()

func _on_Menu_Answers_item_selected(which):
	# Since we're using the menu component, we only have the label of the answer to go on,
	# so we need to figure out which section to go to
	var section = answers[which]
	emit_signal("answer_selected", section)