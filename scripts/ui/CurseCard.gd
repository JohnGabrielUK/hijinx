extends Control

var curse = 0

signal mouseover
signal cursed

onready var offset = $Offset
onready var card_front = $Offset/TextureFront
onready var card_back = $Offset/TextureBack
onready var cursor = $Offset/Cursor
onready var star = $Offset/Star
onready var tween : Tween = $Tween

func hovered():
	card_front.texture.set_region(Rect2(40 * Curses.get_curse_image_index(curse), 0, 40, 56))
	card_front.show()
	card_back.hide()
	cursor.show()
	modulate = Color(1.0, 1.0, 1.0)

func dehovered():
	card_front.hide()
	card_back.show()
	cursor.hide()
	modulate = Color(0.5, 0.5, 0.5)

func set_star_visible(value):
	star.visible = value

func appear() -> void:
	tween.interpolate_property(offset, "rect_position", Vector2(800, 0), Vector2.ZERO, 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	tween.start()
