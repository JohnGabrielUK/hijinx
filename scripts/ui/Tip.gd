extends CanvasLayer

onready var label1 = $HBox/Label1
onready var label2 = $HBox/Label2
onready var tween = $Tween

func get_tips() -> Array:
	var restart_scancode = Options.get_key_binding("restart")
	var restart_label = OS.get_scancode_string(restart_scancode)
	return [
		{
			"part_a": "Tip:",
			"part_b": "You can restart a level instantly\nby pressing " + restart_label + "."
		},
		{
			"part_a": "Tip:",
			"part_b": "There is a secret lair of devious\ntricks and traps, somewhere\non the map..."
		},
		{
			"part_a": "Did you know?",
			"part_b": "Carl's full name is \"Carl\"."
		}
]

func _ready():
	var tips = get_tips()
	label1.text = tips[GameProgress.tips_shown]["part_a"]
	label2.text = tips[GameProgress.tips_shown]["part_b"]
	GameProgress.tips_shown += 1
	tween.interpolate_property(label1, "modulate", Color(1.0, 1.0, 1.0, 0.0), Color(1.0, 1.0, 1.0, 1.0), 1.0, Tween.TRANS_CUBIC, Tween.EASE_OUT, 1.25)
	tween.interpolate_property(label2, "modulate", Color(1.0, 1.0, 1.0, 0.0), Color(1.0, 1.0, 1.0, 1.0), 1.0, Tween.TRANS_CUBIC, Tween.EASE_OUT, 2.75)
	tween.interpolate_property(label1, "modulate", Color(1.0, 1.0, 1.0, 1.0), Color(1.0, 1.0, 1.0, 0.0), 0.5, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT, 5.5)
	tween.interpolate_property(label2, "modulate", Color(1.0, 1.0, 1.0, 1.0), Color(1.0, 1.0, 1.0, 0.0), 0.5, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT, 5.5)
	tween.start()
