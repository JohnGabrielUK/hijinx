extends CanvasLayer

const obj_dialogue = preload("res://objects/ui/DialogueWindow.tscn")

var conversation
var current_section
var conversation_slug

var dialogue
onready var first_line = true

signal done

func load_conversation():
	var file = File.new()
	file.open("res://json/conversations.tscn", file.READ)
	var contents = parse_json(file.get_as_text())
	conversation = contents["conversations"][conversation_slug]
	file.close()

func get_section(section_name):
	return conversation["sections"][section_name]

func get_section_next_type(section):
	return section["next"]["type"]

func start_conversation():
	var current_section_name = conversation["start_point"]
	current_section = get_section(current_section_name)
	do_current_section()

func do_current_section():
	dialogue = obj_dialogue.instance()
	dialogue.lines = current_section["lines"]
	if get_section_next_type(current_section) == "choice":
		dialogue.is_question = true
		dialogue.answers = current_section["next"]["options"]
	else:
		dialogue.is_question = false
	add_child(dialogue)
	dialogue.connect("done", self, "section_done")
	dialogue.connect("answer_selected", self, "answer_selected")
	# Transition in if this is the first line
	if first_line:
		dialogue.appear()
		first_line = false

func goto_next_section():
	# We're done with the last dialogue window, so destroy it
	dialogue.queue_free()
	# Now move onto the next one
	var next_section_name = current_section["next"]["destination"]
	current_section = get_section(next_section_name)
	do_current_section()

func section_done():
	var next_type = get_section_next_type(current_section)
	match next_type:
		"goto": goto_next_section()
		"end": done()

func answer_selected(next_section):
	# We're done with the last dialogue window, so destroy it
	dialogue.queue_free()
	current_section = get_section(next_section)
	do_current_section()

func done():
	dialogue.disappear()
	yield(dialogue, "done_disappearing")
	get_tree().paused = false
	emit_signal("done")
	queue_free()

func _ready():
	get_tree().paused = true
	load_conversation()
	start_conversation()