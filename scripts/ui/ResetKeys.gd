extends Label

var reset_progress : float = 0.0

onready var rect_bar = $ColorRect_Bar_Filled

func _process(delta : float) -> void:
	var actions_pressed : int = 0
	for current_action in Options.actions:
		if Input.is_action_pressed(current_action):
			actions_pressed += 1
	# Are two or more buttons being pressed?
	if actions_pressed > 1:
		reset_progress += delta / 2.0
	else:
		reset_progress = 0.0
	# Are we resetting, then?
	if reset_progress >= 1.0:
		Options.reset_bindings()
		get_tree().call_group("keybinder", "update_key_display")
		reset_progress = 0.0
	rect_bar.rect_scale.x = min(pow(reset_progress, 2.0), 1.0)