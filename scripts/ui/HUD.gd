extends CanvasLayer

onready var hud_bar = $HBox
onready var healthbar = $HBox/TextureProgress_Healthbar
onready var hbox_health = $HBox/HBox_Health
onready var hbox_time = $HBox/HBox_Time
onready var hbox_coins = $HBox/HBox_Coins
onready var label_health = $HBox/HBox_Health/Label_Health
onready var label_time = $HBox/HBox_Time/Label_Time
onready var label_coins = $HBox/HBox_Coins/Label_Coins

var par
var parent

onready var shake_amount = 0
onready var shake_direction = 1

func set_health_visible(value):
	healthbar.visible = value

func set_time_visible(value):
	hbox_time.visible = value

func set_coins_visible(value):
	hbox_coins.visible = value

func shake_hud():
	shake_amount = 0.5

func _process(delta):
	if Curses.get_current_curse() == Curses.TIME_LIMIT or GameSession.is_speedrun():
		var current_time = parent.get_current_time()
		var time = max(par - current_time, 0)
		if GameSession.is_speedrun():
			time = GameSession.speedrun_get_time_elapsed()
		var msec = time % 1000
		var sec = int(floor(time / 1000.0)) % 60
		var minute = floor(time / 60000.0)
		var time_text = "%02d:%02d.%03d" % [minute, sec, msec]
		label_time.text = time_text
	# Update coins
	label_coins.text = "%d/%d" % [parent.get_collected_coins(), parent.get_total_coins()]
	#label_health.text = "%d/3" % parent.player.health
	healthbar.value = parent.player.health
	# Shake it, baby
	if shake_amount > 0:
		if shake_amount > delta:
			shake_amount -= delta
			shake_direction *= -1
		else:
			shake_amount = 0
		hud_bar.rect_position.y = 4 + (pow(shake_amount * 2, 2) * shake_direction * 4)

func connect_to_player(new_player):
	new_player.connect("hit", self, "shake_hud")
