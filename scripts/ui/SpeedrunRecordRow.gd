extends HBoxContainer

onready var label_levelname = $Label_LevelName
onready var label_besttime = $Label_BestTime
onready var label_totaltime = $Label_TotalTime

func set_level_name(value : String) -> void:
	label_levelname.text = value

func set_best_time(value : String) -> void:
	label_besttime.text = value

func set_total_time(value : String) -> void:
	label_totaltime.text = value
