extends CanvasLayer

onready var label_levelname = $Label_LevelName
onready var label_levelprogress = $Label_LevelProgress
onready var label_cardscollected = $HBox_Cards/Label_Cards

func set_level_name(text):
	label_levelname.text = text

func set_level_progress(text):
	label_levelprogress.text = text

func set_cards_collected(text):
	label_cardscollected.text = text