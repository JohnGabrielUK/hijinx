extends CanvasLayer

onready var anim_player = $AnimationPlayer

signal start

func go() -> void:
	get_tree().paused = false
	emit_signal("start")

func _on_AnimationPlayer_animation_finished(anim_name : String) -> void:
	queue_free()

func _ready() -> void:
	anim_player.play("countdown")