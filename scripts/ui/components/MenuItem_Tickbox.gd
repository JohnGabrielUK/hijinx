extends VBoxContainer
tool

const texture_empty = preload("res://sprites/ui/tickbox.png")
const texture_filled = preload("res://sprites/ui/tickbox_filled.png")

onready var label = $HBox/Label
onready var tween = $Tween
onready var tickbox = $HBox/Tickbox

export (String) var label_text setget set_text
export (String) var tickbox_name
export var padding_top = 0 setget set_top_padding
export var ticked = false setget set_ticked

enum Align {LEFT, CENTER, RIGHT}

var ITEM_TYPE = Menu.MenuItemType.ITEM_TICKBOX

func transition_in(delay):
	label.percent_visible = 0
	tickbox.hide()
	yield(get_tree().create_timer(delay), "timeout")
	tickbox.show()
	tween.interpolate_property(label, "percent_visible", 0, 1, 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT_IN)
	tween.start()

func transition_out(delay):
	yield(get_tree().create_timer(delay), "timeout")
	tickbox.hide()
	tween.interpolate_property(label, "percent_visible", 1, 0, 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT_IN)
	tween.start()

func set_text(value):
	if not has_node("HBox/Label"): return # Thanks, WingedAdventurer!
	label_text = value
	$HBox/Label.text = value

func set_top_padding(value):
	$Padding_Top.rect_min_size.y = value
	padding_top = value

func set_ticked(value):
	ticked = value
	if value == true:
		$HBox/Tickbox.texture = texture_filled
	else:
		$HBox/Tickbox.texture = texture_empty

func set_text_align(value):
	match value:
		Align.LEFT: self.alignment = BoxContainer.ALIGN_BEGIN
		Align.CENTER: self.alignment = BoxContainer.ALIGN_CENTER
		Align.RIGHT: self.alignment = BoxContainer.ALIGN_END

func toggle():
	set_ticked(!ticked)
	Options.set_tickbox_value(tickbox_name, ticked)
	Options.apply_tickbox_settings()

func _ready():
	set_ticked(Options.get_tickbox_value(tickbox_name))