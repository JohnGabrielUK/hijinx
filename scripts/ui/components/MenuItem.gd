extends VBoxContainer
tool

onready var label = $Label
onready var tween = $Tween

export (String) var label_text setget set_text
export var padding_top = 0 setget set_top_padding

enum Align {LEFT, CENTER, RIGHT}

var ITEM_TYPE = Menu.MenuItemType.ITEM_BUTTON

func transition_in(delay):
	label.percent_visible = 0
	yield(get_tree().create_timer(delay), "timeout")
	tween.interpolate_property(label, "percent_visible", 0, 1, 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()

func transition_out(delay):
	yield(get_tree().create_timer(delay), "timeout")
	tween.interpolate_property(label, "percent_visible", 1, 0, 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()

func set_text(value):
	if not has_node("Label"): return # Thanks, WingedAdventurer!
	label_text = value
	$Label.text = value

func set_top_padding(value):
	$Padding_Top.rect_min_size.y = value
	padding_top = value

func set_text_align(value):
	match value:
		Align.LEFT: self.alignment = BoxContainer.ALIGN_BEGIN
		Align.CENTER: self.alignment = BoxContainer.ALIGN_CENTER
		Align.RIGHT: self.alignment = BoxContainer.ALIGN_END