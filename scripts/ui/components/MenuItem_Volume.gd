extends VBoxContainer
tool

onready var label = $HBox/Label
onready var volume_bar = $HBox/VolumeBar
onready var tween = $Tween

export (String) var label_text setget set_label_text
export (String) var channel
export var padding_top = 0 setget set_top_padding

enum Align {LEFT, CENTER, RIGHT}

var ITEM_TYPE = Menu.MenuItemType.ITEM_VOLUME

var value

func transition_in(delay):
	label.percent_visible = 0
	volume_bar.hide()
	yield(get_tree().create_timer(delay), "timeout")
	volume_bar.show()
	tween.interpolate_property(label, "percent_visible", 0, 1, 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT_IN)
	tween.start()

func transition_out(delay):
	yield(get_tree().create_timer(delay), "timeout")
	volume_bar.hide()
	tween.interpolate_property(label, "percent_visible", 1, 0, 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT_IN)
	tween.start()

func set_label_text(value):
	if not has_node("HBox/Label"): return # Thanks, WingedAdventurer!
	label_text = value
	$HBox/Label.text = value

func set_top_padding(value):
	$Padding_Top.rect_min_size.y = value
	padding_top = value

func set_text_align(value):
	match value:
		Align.LEFT: self.alignment = BoxContainer.ALIGN_BEGIN
		Align.CENTER: self.alignment = BoxContainer.ALIGN_CENTER
		Align.RIGHT: self.alignment = BoxContainer.ALIGN_END

func increase():
	set_value(value + 1)

func decrease():
	set_value(value - 1)

func set_value(new_value):
	value = clamp(new_value, 0, 7)
	volume_bar.value = new_value
	Options.set_channel_volume(channel, value/7)

func _ready():
	value = Options.get_channel_volume(channel) * 7
	volume_bar.value = value