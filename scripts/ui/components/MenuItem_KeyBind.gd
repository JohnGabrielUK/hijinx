extends VBoxContainer
tool

onready var action_label = $HBox/Label_Action # I know it's inconsistent. Bite me.
onready var label_key = $HBox/Label_Key
onready var texture_button_gamepad = $HBox/TextureRect_Button_Gamepad
onready var tween = $Tween

export (String) var action

export (String) var label_action setget set_label_action
export var padding_top = 0 setget set_top_padding

enum Align {LEFT, CENTER, RIGHT}

var ITEM_TYPE = Menu.MenuItemType.ITEM_KEYBIND

onready var listening = 0

func transition_in(delay):
	action_label.percent_visible = 0
	label_key.percent_visible = 0
	texture_button_gamepad.hide()
	yield(get_tree().create_timer(delay), "timeout")
	texture_button_gamepad.show()
	tween.interpolate_property(action_label, "percent_visible", 0, 1, 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()
	yield(tween, "tween_completed")
	tween.interpolate_property(label_key, "percent_visible", 0, 1, 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()

func transition_out(delay):
	yield(get_tree().create_timer(delay), "timeout")
	tween.interpolate_property(action_label, "percent_visible", 1, 0, 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()
	yield(tween, "tween_completed")
	tween.interpolate_property(label_key, "percent_visible", 1, 0, 0.1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()
	yield(tween, "tween_completed")
	texture_button_gamepad.hide()

func set_label_action(value):
	if not has_node("HBox/Label_Action"): return # Thanks, WingedAdventurer!
	label_action = value
	$HBox/Label_Action.text = value

func set_top_padding(value):
	$Padding_Top.rect_min_size.y = value
	padding_top = value

func set_text_align(value):
	match value:
		Align.LEFT: self.alignment = BoxContainer.ALIGN_BEGIN
		Align.CENTER: self.alignment = BoxContainer.ALIGN_CENTER
		Align.RIGHT: self.alignment = BoxContainer.ALIGN_END

func update_key_display():
	var scancode = Options.get_key_binding(action)
	label_key.text = OS.get_scancode_string(scancode)
	var button = Options.get_gamepad_binding(action)
	texture_button_gamepad.texture.region.position.x = button * 100

# Everything else in the menu uses unhandled_input, so if we're
# listening for a key bind, we'll get first shot
func _input(event):
	if event is InputEventKey:
		# Disregard the release event for the key that started this keybind
		if listening < 3 and listening > 0:
			get_tree().set_input_as_handled()
			listening += 1
		if listening == 3:
			get_tree().set_input_as_handled()
			Options.set_key_binding(action, event.scancode)
			update_key_display()
			listening = 0
	elif event is InputEventJoypadButton:
		# Disregard the release event for the key that started this keybind
		if listening < 3 and listening > 0:
			get_tree().set_input_as_handled()
			listening += 1
		if listening == 3:
			get_tree().set_input_as_handled()
			Options.set_gamepad_binding(action, event.button_index)
			update_key_display()
			listening = 0

func start_binding():
	listening = 1
	label_key.text = "..."

func _ready():
	texture_button_gamepad.texture = texture_button_gamepad.texture.duplicate(true)
	update_key_display()