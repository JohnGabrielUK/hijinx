class_name Menu
extends VBoxContainer
tool

const texture_cursor = preload("res://sprites/ui/cursor.png")

onready var audio_back = $Audio_Back
onready var audio_move = $Audio_Move
onready var audio_select = $Audio_Select
onready var audio_start = $Audio_Start

enum MenuItemType {ITEM_BUTTON, ITEM_TICKBOX, ITEM_VOLUME, ITEM_KEYBIND}

enum Align {LEFT, CENTER, RIGHT}
export (Align) var text_align setget set_text_align

var selected_index = 0
var cursor_bob = 0

var items = []

onready var active = false

signal item_selected
signal cancelled

func transition_in():
	for i in range(0, items.size()):
		items[i].transition_in(i * 0.05)

func transition_out():
	for i in range(0, items.size()):
		items[i].transition_out((items.size() * 0.05) - (i * 0.05))

func set_text_align(value):
	text_align = value
	for next in items:
		next.set_text_align(value)

func _draw():
	if items.size() == 0 or not active: return
	var selected = items[selected_index]
	var cursor_pos = Vector2((sin(cursor_bob * 5) * 1) - 12, selected.rect_position.y + 6 + selected.padding_top)
	draw_texture(texture_cursor, cursor_pos)

func _process(delta):
	cursor_bob += delta
	update()

func next_item():
	selected_index += 1
	if selected_index >= items.size(): selected_index = 0
	audio_move.play()

func previous_item():
	selected_index -= 1
	if selected_index < 0: selected_index = items.size() - 1
	audio_move.play()

func item_selected():
	# Is this a button or a checkbox?
	var selected_item = items[selected_index]
	match selected_item.ITEM_TYPE:
		MenuItemType.ITEM_BUTTON:
			emit_signal("item_selected", selected_item.label_text)
		MenuItemType.ITEM_TICKBOX:
			selected_item.toggle()
		MenuItemType.ITEM_KEYBIND:
			selected_item.start_binding()
	audio_select.play()

func decrease():
	# Only useful on volume bars
	var selected_item = items[selected_index]
	if selected_item.ITEM_TYPE == MenuItemType.ITEM_VOLUME:
		selected_item.decrease()
		audio_move.play()

func increase():
	# Only useful on volume bars
	var selected_item = items[selected_index]
	if selected_item.ITEM_TYPE == MenuItemType.ITEM_VOLUME:
		selected_item.increase()
		audio_move.play()

func cancelled():
	audio_back.play()
	emit_signal("cancelled")

func _input(event):
	# Don't do anything about this if we aren't selected
	if not active:
		return
	# Otherwise, continue, and see if this matches any of the inputs we accept
	var recognised : bool = true
	if event.is_action_pressed("up"):
		previous_item()
	elif event.is_action_pressed("down"):
		next_item()
	elif event.is_action_pressed("left"):
		decrease()
	elif event.is_action_pressed("right"):
		increase()
	elif event.is_action_pressed("jump") or event.is_action_pressed("ui_accept"):
		item_selected()
	elif event.is_action_pressed("interact") or event.is_action_pressed("pause"):
		cancelled()
	else:
		recognised = false
	# Absorb this event if we responded to it
	if recognised: accept_event()

func _enter_tree():
	selected_index = 0
	cursor_bob = 0

func _ready() -> void:
	items = []
	for current_child in get_children():
		if current_child.is_in_group("menu_item"):
			items.append(current_child)
