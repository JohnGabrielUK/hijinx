extends CanvasLayer

const obj_transition = preload("res://objects/ui/Transition.tscn")

onready var label_time = $VBox/Center_Stats/HBox/VBox_Time/Value
onready var label_par = $VBox/Center_Stats/HBox/VBox_Comparison/Value
onready var label_comparison_title = $VBox/Center_Stats/HBox/VBox_Comparison/Label
onready var label_newbest = $VBox/Label_NewBest
onready var vbox_comparison = $VBox/Center_Stats/HBox/VBox_Comparison
onready var audio_clear = $Audio_Clear
onready var audio_blip = $Audio_Blip
onready var menu = $Menu
onready var anim_player = $AnimationPlayer
onready var timer_tick = $Timer_TickUp
onready var timer_backtomap = $Timer_BackToMap

var level
var curse
var clear_time
var comparison_time
var ghost_recording

onready var displayed_clear_time = 0
onready var displayed_par_time = 0

func set_new_best() -> void:
	label_newbest.show()

func time_to_string(time):
	var msec = time % 1000
	var sec = int(floor(time / 1000.0)) % 60
	var minute = floor(time / 60000.0)
	return "%02d:%02d.%03d" % [minute, sec, msec]

func _restart():
	var transition = obj_transition.instance()
	transition.transition_type = 1
	transition.destination_scene = Levels.get_level_scene(level)
	add_child(transition)

func _next():
	var next = Levels.get_level_next(level)
	var scene = Levels.get_level_scene(next)
	var transition = obj_transition.instance()
	transition.transition_type = 1
	transition.destination_scene = scene
	add_child(transition)

func update_save_file():
	GameProgress.set_curse_cleared(level, curse, true, clear_time)
	GameProgress.save_game()

func adjust_display_time(displayed, actual):
	if displayed < actual:
		var difference = actual - displayed
		for current_amount in [10000, 1000, 100, 10, 1]:
			if difference >= current_amount:
				displayed += current_amount
				return displayed
			elif current_amount == 1:
				return actual

func _on_Timer_BackToMap_timeout():
	var transition = obj_transition.instance()
	transition.transition_type = 1
	# Are we going back to the map, or to the next level in a speedrun?
	if GameSession.current_mode == GameSession.GAME_MODE.NORMAL:
		transition.destination_scene = "res://scenes/Map.tscn"
	else:
		# Go to the next level
		var next_level = Levels.get_level_next(level)
		var next_scene = Levels.get_level_scene(next_level) if next_level != null else "res://scenes/SpeedrunIntro.tscn"
		transition.destination_scene = next_scene
	add_child(transition)

func _on_Timer_TickUpTimer_timeout():
	var changed = false
	if displayed_clear_time < clear_time:
		displayed_clear_time = adjust_display_time(displayed_clear_time, clear_time)
		changed = true
	label_time.text = time_to_string(displayed_clear_time)
	if Levels.level_has_curses(level):
		if displayed_par_time < comparison_time:
			displayed_par_time = adjust_display_time(displayed_par_time, comparison_time)
			changed = true
		label_par.text = time_to_string(displayed_par_time)
	if changed:
		timer_tick.start(0.02)
		audio_blip.play()
	else:
		timer_tick.stop()
		timer_backtomap.start(1)

func _ready():
	get_tree().paused = true
	if Levels.level_has_curses(level):
		comparison_time = Levels.get_level_curse_par(level, curse)
		if Levels.get_devs_best_for_curse(level, curse) != -1 and Levels.get_devs_best_for_curse(level, curse) < clear_time:
			label_comparison_title.text = "Dev's best"
			comparison_time = Levels.get_devs_best_for_curse(level, curse)
		elif GameProgress.get_best_time_for_curse(level, Curses.get_curse_slug(curse)) < clear_time:
			label_comparison_title.text = "Your best"
			comparison_time = GameProgress.get_best_time_for_curse(level, Curses.get_curse_slug(curse))
		label_par.text = time_to_string(displayed_par_time)
		if GameProgress.get_best_time_for_curse(level, Curses.get_curse_slug(curse)) > clear_time:
			label_newbest.show()
	else:
		vbox_comparison.hide()
	# Save our progress
	if Levels.level_has_curses(level):
		update_save_file()
		GameProgress.save_ghost_run(level, curse, clear_time, ghost_recording)
	label_time.text = time_to_string(displayed_clear_time)
	audio_clear.play()
	anim_player.play("anim")
	timer_tick.start(1)
