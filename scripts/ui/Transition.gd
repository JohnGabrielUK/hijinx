extends CanvasLayer

onready var texture = $Texture

var actual
var target

export (int, "FADE_IN", "FADE_OUT") var transition_type
export (String) var destination_scene = null

signal transition_done

func _process(delta):
	if target > actual:
		if target > actual + delta:
			actual += delta
		else: actual = target
	# Have we finished?
	if actual == target:
		if transition_type == 0:
			queue_free()
		else:
			get_tree().paused = false
			if destination_scene != null:
				get_tree().change_scene(destination_scene)
			else:
				emit_signal("transition_done")
	else: texture.get_material().set_shader_param("transition", actual)

func _ready():
	actual = 0
	target = 1
	texture.get_material().set_shader_param("transition", actual)
	texture.get_material().set_shader_param("type", transition_type)
