extends Control

onready var menu = $Menu

signal button_video_pressed
signal button_audio_pressed
signal button_controls_pressed
signal button_back_pressed

func set_menu_active(value):
	menu.active = value

func item_selected(which):
	match which:
		"Video":
			emit_signal("button_video_pressed")
		"Audio":
			emit_signal("button_audio_pressed")
		"Controls":
			emit_signal("button_controls_pressed")
		"Back":
			emit_signal("button_back_pressed")

func cancelled():
	emit_signal("button_back_pressed")

func _ready():
	menu.connect("item_selected", self, "item_selected")
	menu.connect("cancelled", self, "cancelled")
