extends Control

const obj_transition = preload("res://objects/ui/Transition.tscn")

onready var main_menu = $CanvasLayer/MainMenu
onready var game_menu = $CanvasLayer/GameMenu
onready var options = $CanvasLayer/OptionsMenu
onready var video_options = $CanvasLayer/VideoMenu
onready var audio_options = $CanvasLayer/AudioMenu
onready var controls = $CanvasLayer/ControlsMenu
onready var credits = $CanvasLayer/Credits
onready var tween = $Tween

onready var bgm = $Audio_BGM
onready var audio_start = $Audio_Start

func lower_background_music() -> void:
#	tween.remove_all()
#	tween.interpolate_property(bgm, "volume_db", 0.0, -50.0, 1.0, Tween.TRANS_BACK, Tween.EASE_IN)
#	tween.interpolate_property(bgm_dimmed, "volume_db", -50.0, 0.0, 1.0, Tween.TRANS_BACK, Tween.EASE_IN)
#	tween.start()
	pass

func raise_background_music() -> void:
#	tween.remove_all()
#	tween.interpolate_property(bgm, "volume_db", -50.0, 0.0, 1.0, Tween.TRANS_BACK, Tween.EASE_IN)
#	tween.interpolate_property(bgm_dimmed, "volume_db", 0.0, -50.0, 1.0, Tween.TRANS_BACK, Tween.EASE_IN)
#	tween.start()
	pass

func play_normal_mode():
	GameSession.current_mode = GameSession.GAME_MODE.NORMAL
	audio_start.play()
	game_menu.set_menu_active(false)
	main_menu.transition_out()
	# Set up the transition
	var transition = obj_transition.instance()
	transition.transition_type = 1
	# Are we starting a new game, or continuing an existing one?
	if GameProgress.new_game:
		transition.destination_scene = "res://levels/Tutorial.tscn"
	else:
		transition.destination_scene = "res://scenes/Map.tscn"
	add_child(transition)

func play_speedrun_mode():
	audio_start.play()
	game_menu.set_menu_active(false)
	main_menu.transition_out()
	# Set up the transition
	var transition = obj_transition.instance()
	transition.transition_type = 1
	# Have we already completed the speedrun?
	if GameProgress.speedrun_complete:
		transition.destination_scene = "res://scenes/SpeedrunIntro.tscn"
	# If not, go straight into things
	else:
		GameSession.current_mode = GameSession.GAME_MODE.SPEEDRUN
		transition.destination_scene = "res://levels/LevelA.tscn"
	add_child(transition)

func show_game_menu():
	main_menu.set_menu_active(false)
	main_menu.transition_out()
	yield(get_tree().create_timer(0.5), "timeout")
	main_menu.hide()
	game_menu.set_menu_active(true)
	game_menu.menu.transition_in()
	game_menu.show()

func hide_game_menu():
	game_menu.set_menu_active(false)
	game_menu.menu.transition_out()
	yield(get_tree().create_timer(0.5), "timeout")
	game_menu.hide()
	main_menu.set_menu_active(true)
	main_menu.transition_in()
	main_menu.show()

func show_options():
	main_menu.set_menu_active(false)
	main_menu.transition_out()
	yield(get_tree().create_timer(0.5), "timeout")
	main_menu.hide()
	options.set_menu_active(true)
	options.menu.transition_in()
	options.show()
	lower_background_music()

func hide_options():
	options.set_menu_active(false)
	options.menu.transition_out()
	yield(get_tree().create_timer(0.5), "timeout")
	options.hide()
	main_menu.set_menu_active(true)
	main_menu.transition_in()
	main_menu.show()
	lower_background_music()

func show_video_options():
	options.set_menu_active(false)
	options.menu.transition_out()
	yield(get_tree().create_timer(0.5), "timeout")
	options.hide()
	video_options.set_menu_active(true)
	video_options.menu.transition_in()
	video_options.show()

func hide_video_options():
	video_options.set_menu_active(false)
	video_options.menu.transition_out()
	yield(get_tree().create_timer(0.5), "timeout")
	video_options.hide()
	options.set_menu_active(true)
	options.menu.transition_in()
	options.show()
	Options.save_settings()

func show_audio_options():
	options.set_menu_active(false)
	options.menu.transition_out()
	yield(get_tree().create_timer(0.5), "timeout")
	options.hide()
	audio_options.set_menu_active(true)
	audio_options.menu.transition_in()
	audio_options.show()

func hide_audio_options():
	audio_options.set_menu_active(false)
	audio_options.menu.transition_out()
	yield(get_tree().create_timer(0.5), "timeout")
	audio_options.hide()
	options.set_menu_active(true)
	options.menu.transition_in()
	options.show()
	Options.save_settings()

func show_controls():
	options.set_menu_active(false)
	options.menu.transition_out()
	yield(get_tree().create_timer(0.5), "timeout")
	options.hide()
	controls.set_menu_active(true)
	controls.menu.transition_in()
	controls.show()

func hide_controls():
	controls.set_menu_active(false)
	controls.menu.transition_out()
	yield(get_tree().create_timer(0.5), "timeout")
	controls.hide()
	options.set_menu_active(true)
	options.menu.transition_in()
	options.show()
	Options.save_settings()

func show_credits():
	main_menu.set_menu_active(false)
	main_menu.transition_out()
	yield(get_tree().create_timer(0.5), "timeout")
	main_menu.hide()
	credits.set_menu_active(true)
	credits.show()
	credits.transition_in()
	lower_background_music()

func hide_credits():
	credits.set_menu_active(false)
	credits.transition_out()
	yield(get_tree().create_timer(0.25), "timeout")
	credits.hide()
	main_menu.set_menu_active(true)
	main_menu.transition_in()
	main_menu.show()
	raise_background_music()

func start_game() -> void:
	if GameProgress.game_finished:
		show_game_menu()
	else:
		play_normal_mode()

func quit():
	get_tree().quit()

func _ready():
	main_menu.connect("button_play_pressed", self, "start_game")
	main_menu.connect("button_options_pressed", self, "show_options")
	main_menu.connect("button_credits_pressed", self, "show_credits")
	main_menu.connect("button_quit_pressed", self, "quit")
	game_menu.connect("button_normal_mode_pressed", self, "play_normal_mode")
	game_menu.connect("button_speedrun_mode_pressed", self, "play_speedrun_mode")
	game_menu.connect("button_back_pressed", self, "hide_game_menu")
	options.connect("button_video_pressed", self, "show_video_options")
	options.connect("button_audio_pressed", self, "show_audio_options")
	options.connect("button_controls_pressed", self, "show_controls")
	options.connect("button_back_pressed", self, "hide_options")
	video_options.connect("button_back_pressed", self, "hide_video_options")
	audio_options.connect("button_back_pressed", self, "hide_audio_options")
	controls.connect("button_back_pressed", self, "hide_controls")
	credits.connect("button_back_pressed", self, "hide_credits")
	main_menu.set_menu_active(true)
	bgm.play()
	#bgm_dimmed.play()
	# Set up the transition
	var transition = obj_transition.instance()
	transition.transition_type = 0
	add_child(transition)
