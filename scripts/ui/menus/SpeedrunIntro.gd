extends Control

const obj_transition = preload("res://objects/ui/Transition.tscn")
const obj_levelrow = preload("res://objects/ui/SpeedrunRecordRow.tscn")

onready var level_times = $CanvasLayer/Center/VBox_LevelTimes
onready var menu = $CanvasLayer/Menu
onready var audio_start = $Audio_Start
onready var audio_bgm = $Audio_BGM

func do_transition():
	var transition = obj_transition.instance()
	transition.transition_type = 0
	add_child(transition)

func time_to_string(time : int) -> String:
	var msec = time % 1000
	var sec = int(floor(time / 1000.0)) % 60
	var minute = floor(time / 60000.0)
	return "%02d:%02d.%03d" % [minute, sec, msec]

func init_table() -> void:
	var total_time = 0
	for current_level in GameProgress.speedrun_segments:
		if Levels.is_level_speedrunnable(current_level):
			var level_name = Levels.get_level_name(current_level)
			var level_time = GameProgress.get_speedrun_segment(current_level)
			var row = obj_levelrow.instance()
			total_time += level_time
			level_times.add_child(row)
			row.set_level_name(level_name)
			row.set_best_time(time_to_string(level_time))
			row.set_total_time(time_to_string(total_time))

func begin_speedrun() -> void:
	menu.active = false
	GameSession.current_mode = GameSession.GAME_MODE.SPEEDRUN
	audio_start.play()
	# Set up the transition
	var transition = obj_transition.instance()
	transition.transition_type = 1
	transition.destination_scene = "res://levels/LevelA.tscn"
	add_child(transition)

func return_to_title() -> void:
	menu.active = false
	var transition = obj_transition.instance()
	transition.transition_type = 1
	transition.destination_scene = "res://scenes/TitleScreen.tscn"
	add_child(transition)

func _on_Menu_item_selected(which : String) -> void:
	match which:
		"Start":
			begin_speedrun()
		"Back to Title":
			return_to_title()

func _ready() -> void:
	do_transition()
	init_table()
	menu.active = true
	audio_bgm.play()
