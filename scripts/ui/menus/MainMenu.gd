extends Control

onready var menu = $Menu
onready var anim_player = $AnimationPlayer

signal button_play_pressed
signal button_options_pressed
signal button_credits_pressed
signal button_quit_pressed

func transition_in():
	anim_player.play("transition_in")
	menu.transition_in()

func transition_out():
	anim_player.play("transition_out")
	menu.transition_out()

func item_selected(which):
	match which:
		"Play":
			emit_signal("button_play_pressed")
		"Options":
			emit_signal("button_options_pressed")
		"Credits":
			emit_signal("button_credits_pressed")
		"Quit":
			emit_signal("button_quit_pressed")

func cancelled():
	pass

func set_menu_active(value):
	menu.active = value

func _ready():
	menu.connect("item_selected", self, "item_selected")
	menu.connect("cancelled", self, "cancelled")
