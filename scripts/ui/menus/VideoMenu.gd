extends Control

onready var menu = $Menu

signal button_back_pressed

func set_menu_active(value):
	menu.active = value
	menu.selected_index = 0

func item_selected(which):
	match which:
		"Back":
			emit_signal("button_back_pressed")

func cancelled():
	emit_signal("button_back_pressed")

func _ready():
	menu.connect("item_selected", self, "item_selected")
	menu.connect("cancelled", self, "cancelled")
