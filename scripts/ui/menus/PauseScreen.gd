extends CanvasLayer

const obj_transition = preload("res://objects/ui/Transition.tscn")

onready var pause_menu = $PauseMenu
onready var options_menu = $OptionsMenu
onready var video_options = $VideoMenu
onready var audio_options = $AudioMenu
onready var controls_menu = $ControlsMenu
onready var darken = $Darken
onready var tween = $Tween

var parent

func resume():
	tween.interpolate_property(darken, "modulate", Color.white, Color(1.0, 1.0, 1.0, 0.0), 0.05, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.interpolate_property(pause_menu, "modulate", Color.white, Color(1.0, 1.0, 1.0, 0.0), 0.05, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
	yield(tween, "tween_all_completed")
	get_tree().paused = false
	parent.unpause()
	queue_free()

func show_options():
	options_menu.set_menu_active(true)
	options_menu.show()
	pause_menu.set_menu_active(false)
	pause_menu.hide()

func hide_options():
	options_menu.set_menu_active(false)
	options_menu.hide()
	pause_menu.set_menu_active(true)
	pause_menu.show()

func show_video_options():
	options_menu.set_menu_active(false)
	options_menu.hide()
	video_options.set_menu_active(true)
	video_options.show()

func hide_video_options():
	video_options.set_menu_active(false)
	video_options.hide()
	options_menu.set_menu_active(true)
	options_menu.show()
	Options.save_settings()

func show_audio_options():
	options_menu.set_menu_active(false)
	options_menu.hide()
	audio_options.set_menu_active(true)
	audio_options.show()

func hide_audio_options():
	audio_options.set_menu_active(false)
	audio_options.hide()
	options_menu.set_menu_active(true)
	options_menu.show()
	Options.save_settings()

func show_controls_menu():
	options_menu.set_menu_active(false)
	options_menu.hide()
	controls_menu.set_menu_active(true)
	controls_menu.show()

func hide_controls_menu():
	controls_menu.set_menu_active(false)
	controls_menu.hide()
	options_menu.set_menu_active(true)
	options_menu.show()
	Options.save_settings()

func change_curse():
	pause_menu.set_menu_active(false)
	pause_menu.hide()
	var transition = obj_transition.instance()
	transition.transition_type = 1
	transition.destination_scene = get_tree().current_scene.filename
	add_child(transition)

func return_to_map():
	SpeedrunMusic.speedrun_finish()
	pause_menu.set_menu_active(false)
	pause_menu.hide()
	var transition = obj_transition.instance()
	transition.transition_type = 1
	transition.destination_scene = "res://scenes/Map.tscn"
	add_child(transition)

func return_to_title():
	SpeedrunMusic.speedrun_finish()
	pause_menu.set_menu_active(false)
	pause_menu.hide()
	var transition = obj_transition.instance()
	transition.transition_type = 1
	transition.destination_scene = "res://scenes/TitleScreen.tscn"
	add_child(transition)

func _unhandled_input(event):
	if event is InputEvent:
		if event.is_action_pressed("pause"):
			resume()

func _ready():
	pause_menu.connect("button_resume_pressed", self, "resume")
	pause_menu.connect("button_options_pressed", self, "show_options")
	pause_menu.connect("button_change_curse_pressed", self, "change_curse")
	pause_menu.connect("button_return_to_map_pressed", self, "return_to_map")
	pause_menu.connect("button_return_to_title_pressed", self, "return_to_title")
	options_menu.connect("button_video_pressed", self, "show_video_options")
	options_menu.connect("button_audio_pressed", self, "show_audio_options")
	options_menu.connect("button_controls_pressed", self, "show_controls_menu")
	options_menu.connect("button_back_pressed", self, "hide_options")
	video_options.connect("button_back_pressed", self, "hide_video_options")
	audio_options.connect("button_back_pressed", self, "hide_audio_options")
	controls_menu.connect("button_back_pressed", self, "hide_controls_menu")
	pause_menu.set_menu_active(true)
	get_tree().paused = true
	tween.interpolate_property(darken, "modulate", Color(1.0, 1.0, 1.0, 0.0), Color.white, 0.05, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.interpolate_property(pause_menu, "modulate", Color(1.0, 1.0, 1.0, 0.0), Color.white, 0.05, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
