extends Control

onready var menu = $Menu
onready var tween = $Tween
onready var back = $ColorRect_Back
onready var label_credits = $Label_Credits

signal button_back_pressed

func set_menu_active(value):
	menu.active = value

func item_selected(value):
	emit_signal("button_back_pressed")

func cancelled():
	emit_signal("button_back_pressed")

func transition_in():
	tween.interpolate_property(self, "modulate", Color(1.0, 1.0, 1.0, 0.0), Color.white, 0.2, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()

func transition_out():
	tween.interpolate_property(self, "modulate", Color.white, Color(1.0, 1.0, 1.0, 0.0), 0.2, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()

func _ready():
	menu.connect("item_selected", self, "item_selected")
	menu.connect("cancelled", self, "cancelled")
