extends Control

onready var menu = $Menu

signal button_normal_mode_pressed
signal button_speedrun_mode_pressed
signal button_back_pressed

func set_menu_active(value):
	menu.active = value

func item_selected(which):
	match which:
		"Normal Mode":
			emit_signal("button_normal_mode_pressed")
		"Speedrun Mode":
			emit_signal("button_speedrun_mode_pressed")
		"Back":
			emit_signal("button_back_pressed")

func cancelled():
	emit_signal("button_back_pressed")

func _ready():
	menu.connect("item_selected", self, "item_selected")
	menu.connect("cancelled", self, "cancelled")
