extends Control

onready var menu = $Menu

signal button_resume_pressed
signal button_options_pressed
signal button_change_curse_pressed
signal button_return_to_map_pressed
signal button_return_to_title_pressed

func item_selected(which):
	match which:
		"Resume":
			emit_signal("button_resume_pressed")
		"Options":
			emit_signal("button_options_pressed")
		"Change Curse":
			emit_signal("button_change_curse_pressed")
		"Return to Map":
			emit_signal("button_return_to_map_pressed")
		"Return to Title":
			emit_signal("button_return_to_title_pressed")

func cancelled():
	emit_signal("button_resume_pressed")

func set_menu_active(value):
	menu.active = value

func _ready():
	menu.connect("item_selected", self, "item_selected")
	menu.connect("cancelled", self, "cancelled")
