extends Node

onready var bgm = $BGM

func speedrun_start() -> void:
	if not bgm.playing:
		bgm.play()

func speedrun_finish() -> void:
	bgm.stop()
