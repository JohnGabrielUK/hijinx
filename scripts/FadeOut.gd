extends CanvasLayer

onready var anim = $AnimationPlayer
onready var rect = $ColorRect

export (String) var destination_scene

func fade_out():
	get_tree().paused = true
	rect.visible = true
	anim.play("FadeOut")

func _animation_finished(anim_name):
	get_tree().paused = false
	get_tree().change_scene(destination_scene)
