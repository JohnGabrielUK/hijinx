extends TileMap

class_name LevelGeometry

const obj_deadly = preload("res://objects/Deadly.tscn")
const obj_torch = preload("res://objects/NewLight.tscn")
const obj_coin = preload("res://objects/Coin.tscn")
const obj_flame = preload("res://objects/effects/TorchFlame.tscn")
const obj_floor_warning = preload("res://objects/FlashWarning.tscn")

const SPIKE_IDS = [5, 8]
const TORCH_TYPES = {
	6: {
		"colour": Color("8066521d"),
		"size": 128,
		"flame": true
	},
	7: {
		"colour": Color("80857439"),
		"size": 128,
		"flame": false
	},
	9: {
		"colour": Color("801d4d74"),
		"size": 128,
		"flame": false
	}
}
const COIN_ID = 4

const TILE_SIZE = 16

var warning

export (bool) var show_coins = false # By default, coins are hidden unless the relevant curse is enabled; for demos, we want to show them regardless

func _draw():
	pass

func update_lights():
	for next_light in get_tree().get_nodes_in_group("light"):
		next_light.visible = Options.get_complex_lights()

func remove_coins():
	for next_coin in get_tree().get_nodes_in_group("coin"):
		next_coin.queue_free()

func get_level_width():
	return (get_used_rect().size.x) * cell_size.x * scale.x

func get_level_height():
	return (get_used_rect().size.y) * cell_size.y * scale.y

func place_light(positions, colour, size, flame):
	for next in positions:
		var new_object = obj_torch.instance()
		var pos_x = (next[0]*TILE_SIZE) + (TILE_SIZE/2)
		var pos_y = (next[1]*TILE_SIZE) + (TILE_SIZE/2)
		new_object.global_position = Vector2(pos_x, pos_y)
		new_object.light_colour = colour
		new_object.light_size = size
		#new_object.flame_visible = flame
		if flame:
			var new_flame = obj_flame.instance()
			new_flame.global_position = Vector2(pos_x, pos_y)
			add_child(new_flame)
		add_child(new_object)

func place_objects(object, positions, remove_tile=false):
	for next in positions:
		var new_object = object.instance()
		var pos_x = (next[0]*TILE_SIZE) + (TILE_SIZE/2)
		var pos_y = (next[1]*TILE_SIZE) + (TILE_SIZE/2)
		new_object.global_position = Vector2(pos_x, pos_y)
		add_child(new_object)
		if remove_tile: set_cell(next[0], next[1], -1)

func make_floor_warning() -> void: # Visual indicator for "the floor is lava"
	warning = obj_floor_warning.instance()
	add_child(warning)
	# Add our tiles to the floor warning
	for current_tile_type in [0, 1, 2]:
		for current_pos in get_used_cells_by_id(current_tile_type):
			if get_cellv(current_pos + Vector2.UP) == -1 and current_pos.y != 0:
				warning.set_cellv(current_pos, 0)
				warning.set_cellv(current_pos + Vector2.UP, 1)

func build_level():
	var coin_positions = get_used_cells_by_id(COIN_ID)
	for current_id in TORCH_TYPES:
		var torch_positions = get_used_cells_by_id(current_id)
		var torch_colour = TORCH_TYPES[current_id]["colour"]
		var torch_size = TORCH_TYPES[current_id]["size"]
		var torch_flame = TORCH_TYPES[current_id]["flame"]
		place_light(torch_positions, torch_colour, torch_size, torch_flame)
	for current_id in SPIKE_IDS:
		var spike_positions = get_used_cells_by_id(current_id)
		place_objects(obj_deadly, spike_positions)
	place_objects(obj_coin, coin_positions, true)
	update_lights()

func _enter_tree():
	build_level()
