extends Node2D

const obj_pausescreen = preload("res://objects/ui/SimplePauseScreen.tscn")
const obj_transition = preload("res://objects/ui/Transition.tscn")
const obj_tip = preload("res://objects/ui/Tip.tscn")

export (NodePath) var path_player

onready var player = get_node(path_player)
onready var ui = $MapUI
onready var water = $Sprite_Water
onready var audio_ding = $Audio_Ding
onready var audio_enter_level = $Audio_EnterLevel
onready var bgm = $Audio_BGM
onready var tween = $Tween

onready var last_ding_pos = Vector2.ZERO # I swear, this makes sense in context
onready var active : bool = true

func pause():
	var pause = obj_pausescreen.instance()
	pause.parent = self
	add_child(pause)

func unpause():
	pass

func do_transition():
	var transition = obj_transition.instance()
	transition.transition_type = 0
	add_child(transition)

func enter_level(level_slug):
	if not active: return
	active = false
	audio_enter_level.play()
	# Fade out the music volume
	tween.interpolate_property(bgm, "volume_db", 0, -50, 1.0, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()
	# Save our current map position
	GameProgress.map_position = player.global_position
	# Now setup the transition
	var scene = Levels.get_level_scene(level_slug)
	var transition = obj_transition.instance()
	transition.transition_type = 1
	add_child(transition)
	# Setup tips
	if GameProgress.tips_shown < GameProgress.TOTAL_TIPS and Options.get_tickbox_value("show_tips"):
		var tip = obj_tip.instance()
		add_child(tip)
		yield(get_tree().create_timer(6.0), "timeout")
		get_tree().change_scene(scene)
	else:
		yield(transition, "transition_done")
		get_tree().change_scene(scene)
	

func _input(event):
	if event.is_action_pressed("jump") or event.is_action_pressed("ui_accept"):
		if player.moving == false:
			# Which player is the player on?
			for current_node in get_tree().get_nodes_in_group("map_node"):
				if player.global_position == current_node.global_position:
					enter_level(current_node.level_slug)
	if event.is_action_pressed("pause"):
		get_tree().set_input_as_handled()
		pause()

func set_ui_for_level(slug):
	ui.set_level_name(Levels.get_level_name(slug))
	if Levels.get_level_curses(slug) != null:
		var cards_collected = GameProgress.get_curses_cleared_for_level(slug)
		var total_cards = Levels.get_level_curses(slug).size()
		ui.set_level_progress("%d out of %d cards collected" % [cards_collected, total_cards])
		if last_ding_pos != player.global_position:
			audio_ding.play()
			last_ding_pos = player.global_position
	else:
		ui.set_level_progress("")

func _process(delta):
	# Is the player standing on a level node?
	ui.set_level_name("")
	ui.set_level_progress("")
	for current_node in get_tree().get_nodes_in_group("map_node"):
		if player.global_position == current_node.global_position:
			var slug = current_node.level_slug
			set_ui_for_level(slug)
	water.region_rect.position.x -= delta * 2

func _ready():
	# Update the save so we start from here from now on
	GameProgress.new_game = false
	GameProgress.save_game()
	# Now go ahead and set everything up
	player.global_position = GameProgress.map_position
	do_transition()
	var cards = GameProgress.get_curses_cleared()
	var total = GameProgress.get_total_curses()
	ui.set_cards_collected("%d/%d" % [cards, total])
