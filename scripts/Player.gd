extends KinematicBody2D

class_name Player

const sounds_run_brick : Array = [
	preload("res://sounds/player/steps/Footsteps_Brick1.wav"),
	preload("res://sounds/player/steps/Footsteps_Brick2.wav"),
	preload("res://sounds/player/steps/Footsteps_Brick3.wav"),
	preload("res://sounds/player/steps/Footsteps_Brick4.wav"),
	preload("res://sounds/player/steps/Footsteps_Brick5.wav")
]
const sounds_run_tube : Array = [
	preload("res://sounds/player/steps/Footsteps_Pipe1.wav"),
	preload("res://sounds/player/steps/Footsteps_Pipe2.wav"),
	preload("res://sounds/player/steps/Footsteps_Pipe3.wav"),
	preload("res://sounds/player/steps/Footsteps_Pipe4.wav"),
	preload("res://sounds/player/steps/Footsteps_Pipe5.wav")
]
const sounds_run_dirt : Array = [
	preload("res://sounds/player/steps/Footsteps_Dirt1.wav"),
	preload("res://sounds/player/steps/Footsteps_Dirt2.wav"),
	preload("res://sounds/player/steps/Footsteps_Dirt3.wav"),
	preload("res://sounds/player/steps/Footsteps_Dirt4.wav"),
	preload("res://sounds/player/steps/Footsteps_Dirt5.wav")
]

const sounds_land : Array = [
	preload("res://sounds/player/steps/Footsteps_Brick_Land.wav"),
	preload("res://sounds/player/steps/Footsteps_Pipe_Land.wav"),
	preload("res://sounds/player/steps/Footsteps_Dirt_Land.wav")
]

const obj_rundust = preload("res://objects/effects/RunDust.tscn")
const obj_landdust = preload("res://objects/effects/LandDust.tscn")
const obj_slidedust = preload("res://objects/effects/SlideDust.tscn")

onready var sprite = $AnimatedSprite
onready var collision = $CollisionShape2D
onready var audio_jump = $Audio_Jump
onready var audio_land = $Audio_Land
onready var audio_step = $Audio_Step
onready var audio_slide = $Audio_Slide
onready var audio_spiked = $Audio_Spiked
onready var audio_fall = $Audio_Fall
onready var audio_leavewater = $Audio_LeaveWater
onready var audio_underwater = $Audio_Underwater
onready var throwpoint = $Throwpoint
onready var area_interactables = $Area2D_Interactables
onready var position_feet = $Position2D_Feet
onready var position_slide = $Position2D_Slide
onready var particles_drip = $Particles2D_Drip
onready var area_hitbox = $Area2D_Hitbox
onready var raycast_floor = $RayCast2D_Floor

enum STATE {STANDING, JUMPING, FALLING, WALLGRAB, WALLKICK, HURT, DEAD}
enum FLOOR_TYPE {BRICK, TUBE, DIRT}

const ANIMATIONS = ["fall", "hurt", "idle1", "idle2", "jump", "land", "run", "stand", "wallgrab", "wallkick"]

const RUN_SPEED = 104
const DRAG_SPEED = 1024
const JUMP_SPEED = 236
const FALL_INCR = 386
const MAX_FALL_SPEED = 386
const THROW_SPEED = 128
const WALLSLIDE_MODIFIER = 0.25
const WALLJUMP_GRACE_TIME = 0.1 # For how many seconds after letting go of a wall can we still jump?
const SLIDEDUST_REFRESH_TIME = 0.2 # How many seconds between dust when sliding?

var run_velocity = Vector2.ZERO
var jump_velocity = Vector2.ZERO
var gravity_direction = Vector2.DOWN
var facing = Vector2.RIGHT

var health = 3
var state = STATE.STANDING
var vulnerable = true
var in_water = false
var jump_grace_timer = 0
var drip_amount = 0
var slidedust_refresh = 0 # How long until we next emit sliding dust?
var idle_time = 0 # How long have we spent idling?
var step_sound_index : int = 0
var current_floor_type : int = 0

var camera : Camera2D
var level_bounds : Vector2 = Vector2(1000, 1000)
onready var current_camera_zone : int = -1

var ghost_recording = []

signal hit # Used by HUD for shaking
signal died

func record_ghost_position() -> Dictionary:
	return {
		"position": global_position,
		"flip_h": sprite.flip_h,
		"flip_v": sprite.flip_v,
		"animation": ANIMATIONS.find(sprite.animation),
		"frame": sprite.frame
	}

func set_camera_bounds(id : int, top_left : Vector2, bottom_right : Vector2):
	camera.limit = Rect2(top_left, bottom_right - top_left)
	current_camera_zone = id

func set_camera_position(position : Vector2) -> void:
	if camera != null:
		camera.target_position = position

func unset_camera_bounds(id : int):
	if current_camera_zone == id:
		# See if we can find a new zone
		for current_zone in get_tree().get_nodes_in_group("camera_zone"):
			if current_zone.overlaps_body(self) and current_zone.id != current_camera_zone:
				set_camera_bounds(current_zone.id, current_zone.global_position,
					current_zone.corner.global_position)
				return
		# No? Then default to free camera
		camera.limit = Rect2(0, 0, level_bounds.x, level_bounds.y)
		current_camera_zone -1

func change_gravity(grav):
	gravity_direction = grav
	sprite.flip_v = grav.y == -1
	if grav == Vector2.DOWN:
		position_feet.position.y = 27
		position_slide.position.y = 18
	else:
		position_feet.position.y = -19
		position_slide.position.y = -12

func enter_water():
	sprite.speed_scale = 0.5
	in_water = true
	drip_amount = 0
	particles_drip.emitting = false

func exit_water():
	sprite.speed_scale = 1
	audio_leavewater.play()
	in_water = false
	drip_amount = 5
	particles_drip.emitting = true

func _head_submerged(area):
	if area.is_in_group("water"):
		audio_underwater.play()
		for next_bus in ["SFX", "BGM", "AMB"]:
			AudioServer.set_bus_effect_enabled(AudioServer.get_bus_index(next_bus), 0, true)

func _head_emerge(area):
	if area.is_in_group("water"):
		audio_underwater.stop()
		for next_bus in ["SFX", "BGM", "AMB"]:
			AudioServer.set_bus_effect_enabled(AudioServer.get_bus_index(next_bus), 0, false)

func drop_camera():
	#remove_child(camera)
	#camera.global_position = global_position
	#get_parent().add_child(camera)
	pass

func set_level_bounds(x, y):
	camera.limit = Rect2(0, 0, x, y)
	level_bounds = Vector2(x, y)

func get_run_speed():
	if in_water:
		return RUN_SPEED / 1.5
	return RUN_SPEED

func get_drag_speed():
	return DRAG_SPEED

func get_jump_speed():
	if in_water:
		return JUMP_SPEED / 1.75
	return JUMP_SPEED

func get_fall_incr():
	if in_water:
		return FALL_INCR / 4
	return FALL_INCR

func get_max_fall_speed():
	if in_water:
		return MAX_FALL_SPEED / 4
	return MAX_FALL_SPEED

func facing_left():
	return sprite.flip_h

func turn_sideways(direction):
	sprite.flip_h = direction
	if direction:
		facing = Vector2.LEFT
	else:
		facing = Vector2.RIGHT

func process_tilemap_collision(collider, collision_position):
	for current_offset in [Vector2.ZERO, Vector2(-8, 0)]:
		var tile_pos = collider.world_to_map(collision_position + current_offset)
		var tile_type = collider.get_cell(tile_pos.x, tile_pos.y)
		if tile_type != -1:
			current_floor_type = tile_type
			if tile_type in [0, 1, 2] and !audio_land.playing:
				audio_land.stream = sounds_land[tile_type]

func slow_running(delta):
	# Slow the player down
	var run_decrement = get_drag_speed() * delta
	if run_velocity.x > run_decrement: run_velocity.x -= run_decrement
	elif run_velocity.x > 0: run_velocity.x = 0
	if run_velocity.x < -run_decrement: run_velocity.x += run_decrement
	elif run_velocity.x < 0: run_velocity.x = 0

func jump_movement(delta, fall_modifier = 1.0):
	jump_velocity.y = clamp(jump_velocity.y + (get_fall_incr() * gravity_direction.y * delta), -get_max_fall_speed() * fall_modifier, get_max_fall_speed() * fall_modifier)
	move_and_slide(jump_velocity, gravity_direction * -1)
	if is_on_ceiling():
		if gravity_direction == Vector2.DOWN:
			jump_velocity.y = max(0.0, jump_velocity.y)
		else:
			jump_velocity.y = min(0.0, jump_velocity.y)

func standing_run(delta):
	if Input.is_action_pressed("right"):
		run_velocity.x = get_run_speed()
		turn_sideways(false)
		sprite.play("run")
	elif Input.is_action_pressed("left"):
		run_velocity.x = -get_run_speed()
		turn_sideways(true)
		sprite.play("run")
	else:
		if sprite.animation == "run":
			sprite.play("stand")
			idle_time = 0
		# Slow the player down
		slow_running(delta)
	move_and_slide(run_velocity, gravity_direction * -1.0)
	# Change running sounds
	if raycast_floor.is_colliding():
		var collider = raycast_floor.get_collider()
		if collider is LevelGeometry:
			process_tilemap_collision(raycast_floor.get_collider(), raycast_floor.get_collision_point())

func jumping_run(delta):
	if Input.is_action_pressed("right"):
		run_velocity.x = get_run_speed()
		turn_sideways(false)
	elif Input.is_action_pressed("left"):
		run_velocity.x = -get_run_speed()
		turn_sideways(true)
	else:
		# Slow the player down
		slow_running(delta)
	move_and_slide(run_velocity, gravity_direction * -1.0)

func standing(delta):
	standing_run(delta)
	# Idle animation
	idle_time += delta
	if int(idle_time) % 10 > 8:
		if sprite.animation == "idle1":
			sprite.play("idle2")
	elif int(idle_time) % 10 > 5:
		if sprite.animation == "stand":
			sprite.play("idle1")
	if Input.is_action_just_pressed("interact"):
		#pickup()
		interact()
	# Check if we're on the floor
	if test_move(transform, gravity_direction):
		if Input.is_action_just_pressed("jump"):
			jump()
	else:
		fall()

func jumping(delta):
	jumping_run(delta)
	jump_movement(delta)
	if Input.is_action_just_pressed("interact"):
		interact()
	if Input.is_action_just_released("jump"):
		if gravity_direction == Vector2.DOWN:
			jump_velocity.y = max(jump_velocity.y, -4.0)
		else:
			jump_velocity.y = min(jump_velocity.y, 4.0)
		fall()
	else:
		if gravity_direction == Vector2.DOWN and jump_velocity.y > 0:
			fall()
		if gravity_direction == Vector2.UP and jump_velocity.y < 0:
			fall()

func falling(delta):
	jumping_run(delta)
	jump_movement(delta)
	jump_grace_timer -= delta
	if Input.is_action_just_pressed("jump") && jump_grace_timer > 0:
		jump()
	if Input.is_action_just_pressed("interact"):
		interact()
	# Check if we're touching the wall
	elif test_move(transform, facing):
		grab_wall()
	elif is_on_floor():
		land()

func grabbing_wall(delta):
	jumping_run(delta)
	jump_movement(delta, WALLSLIDE_MODIFIER)
	# Emit sliding dust
	slidedust_refresh -= delta
	if slidedust_refresh <= 0 and not in_water:
		var dust = obj_slidedust.instance()
		get_parent().add_child(dust)
		dust.global_position = position_slide.global_position
		dust.scale.y = gravity_direction.y
		if sprite.flip_h:
			dust.global_position.x = global_position.x - position_slide.position.x
			dust.flip_h = true
		slidedust_refresh = SLIDEDUST_REFRESH_TIME
	if Input.is_action_just_pressed("jump"):
		kick_wall()
	elif is_on_floor():
		land()
	elif not test_move(transform, facing):
		jump_grace_timer = WALLJUMP_GRACE_TIME # For a split-second after we're let go of the wall, we can still jump
		fall()

func kicking_wall(delta):
	if sprite.flip_h: run_velocity.x = get_run_speed()/2
	else: run_velocity.x = -get_run_speed()/2
	move_and_slide(run_velocity, gravity_direction * -1.0)
	jump_movement(delta)
	if Input.is_action_just_released("jump"):
		if gravity_direction == Vector2.DOWN:
			jump_velocity.y = max(jump_velocity.y, -4.0)
		else:
			jump_velocity.y = min(jump_velocity.y, 4.0)
		fall()

func flinching(delta):
	if sprite.flip_h: run_velocity.x = get_run_speed()/2
	else: run_velocity.x = -get_run_speed()/2
	move_and_slide(run_velocity, gravity_direction * -1)
	jump_movement(delta)

func jump():
	# Are we cursed?
	if Curses.get_current_curse() == Curses.CANNOT_JUMP:
		return
	jump_velocity.y = -get_jump_speed() * gravity_direction.y
	sprite.play("jump")
	audio_jump.play()
	state = STATE.JUMPING
 
func fall():
	sprite.play("fall")
	state = STATE.FALLING

func land():
	# Are we cursed?
	if Curses.get_current_curse() == Curses.CANNOT_LAND:
		die()
	else:
		sprite.play("land")
		jump_velocity.x = 0
		jump_velocity.y = 0
		state = STATE.STANDING
		# Change the sound effects for landing
		for i in range(0, get_slide_count()):
			var collision : KinematicCollision2D = get_slide_collision(i)
			if collision.collider is LevelGeometry:
				var geometry : TileMap = collision.collider
				process_tilemap_collision(geometry, collision.position)
		audio_land.call_deferred("play")
		# Kick up some dust
		if not in_water:
			for current_flip in [1, -1]:
				var dust = obj_landdust.instance()
				get_parent().add_child(dust)
				dust.global_position = position_feet.global_position
				dust.global_position.x += 12 * current_flip
				dust.flip_h = (current_flip == 1)
				dust.scale.y = gravity_direction.y

func grab_wall():
	# Are we cursed?
	if Curses.get_current_curse() == Curses.CANNOT_WALLGRIP:
		return
	sprite.play("wallgrab")
	state = STATE.WALLGRAB
	audio_slide.play()
	slidedust_refresh = 0

func kick_wall():
	# Are we cursed?
	if Curses.get_current_curse() == Curses.CANNOT_JUMP:
		return
	sprite.play("wallkick")
	jump_velocity.y = -get_jump_speed() * gravity_direction.y
	state = STATE.WALLKICK
	audio_jump.play()

func interact():
	for next in area_interactables.get_overlapping_areas():
		next.interact()

func pickup():
	sprite.play("hold")
	state = STATE.HOLDING

func become_vulnerable():
	vulnerable = true

func recover():
	if is_on_floor():
		land()
	else:
		fall()
	var timer = Timer.new()
	add_child(timer)
	timer.connect("timeout", self, "become_vulnerable")
	timer.one_shot = true
	timer.start(2)

func hit():
	# Are we cursed?
	if Curses.get_current_curse() in [Curses.WEAK, Curses.JERK]:
		die()
		return
	if !vulnerable: return
	# Does this hit kill us?
	health -= 1
	if health == 0:
		die()
		return
	vulnerable = false
	emit_signal("hit")
	sprite.play("hurt")
	setup_recovery()
	# Jump backwards
	jump_velocity.y = -50 * gravity_direction.y
	state = STATE.HURT
	audio_spiked.play()

func die():
	if state == STATE.DEAD: return
	drop_camera()
	audio_spiked.play()
	vulnerable = false
	collision.disabled = true
	sprite.play("hurt")
	jump_velocity.y = -50 * gravity_direction.y
	state = STATE.DEAD
	emit_signal("died")

func out_of_bounds():
	audio_fall.play()
	hide()
	state = STATE.DEAD
	emit_signal("died")

func setup_recovery():
	var timer = Timer.new()
	add_child(timer)
	timer.connect("timeout", self, "recover")
	timer.one_shot = true
	timer.start(0.5)

func in_level_bounds():
	return position.x > -16 and position.x < level_bounds.x + 16 and position.y > -16 and position.y < level_bounds.y + 16

func _input(event : InputEvent) -> void:
	if event.is_action_pressed("restart"):
		die()

func _physics_process(delta):
	match state:
		STATE.STANDING: standing(delta)
		STATE.JUMPING: jumping(delta)
		STATE.FALLING: falling(delta)
		STATE.WALLGRAB: grabbing_wall(delta)
		STATE.WALLKICK: kicking_wall(delta)
		STATE.HURT: flinching(delta)
		STATE.DEAD: flinching(delta)
	# Check for ouchies
	if area_hitbox.get_overlapping_areas().size() != 0:
		hit()
	# Stop the slide sound if we're not sliding
	if state != STATE.WALLGRAB:
		audio_slide.stop()
	# Are we out of bounds?
	if not in_level_bounds() and state != STATE.DEAD:
		out_of_bounds()
	# If not dead, make the camera follow us
	if state != STATE.DEAD:
		set_camera_position(global_position)
	# If we're wet, drip
	if drip_amount > 0:
		drip_amount -= delta
		if drip_amount <= 0:
			particles_drip.emitting = false
	# Record ghost
	ghost_recording.append(record_ghost_position())

func _animation_finished():
	if sprite.animation == "idle2" and state == STATE.STANDING:
		sprite.play("stand")
		idle_time = 0
	elif sprite.animation == "land":
		if state == STATE.STANDING:
			sprite.play("stand")
			idle_time = 0
		elif state == STATE.HOLDING:
			sprite.play("hold")
	elif state == STATE.WALLKICK:
		state = STATE.JUMPING

func blink():
	if !vulnerable:
		if sprite.modulate.a == 1.0: sprite.modulate.a = 0.25
		else: sprite.modulate.a = 1.0
	else:
		sprite.modulate.a = 1.0

func play_footstep() -> void:
	match current_floor_type:
		FLOOR_TYPE.BRICK: audio_step.stream = sounds_run_brick[step_sound_index]
		FLOOR_TYPE.TUBE: audio_step.stream = sounds_run_tube[step_sound_index]
		FLOOR_TYPE.DIRT: audio_step.stream = sounds_run_dirt[step_sound_index]
	step_sound_index = wrapi(step_sound_index + 1, 0, 4)
	audio_step.play()

func _on_AnimatedSprite_frame_changed():
	if sprite.animation == "run" and sprite.frame in [3, 7]:
		play_footstep()
		if not in_water:
			# Kick up some dust
			var rundust = obj_rundust.instance()
			rundust.global_position = position_feet.global_position
			rundust.flip_h = sprite.flip_h
			rundust.scale.y = gravity_direction.y
			get_parent().add_child(rundust)
