extends AnimatedSprite

const ANIMATIONS = ["fall", "hurt", "idle1", "idle2", "jump", "land", "run", "stand", "wallgrab", "wallkick"]

var ghost_recording : Array
var ghost_frame = 0

func _physics_process(delta):
	if ghost_frame >= ghost_recording.size():
		return
	var current_frame = ghost_recording[ghost_frame]
	global_position = current_frame["position"]
	flip_h = current_frame["flip_h"]
	flip_v = current_frame["flip_v"]
	animation = ANIMATIONS[current_frame["animation"]]
	frame = current_frame["frame"]
	ghost_frame += 1

func reset():
	ghost_frame = 0
