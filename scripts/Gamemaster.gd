extends Node

const obj_transition = preload("res://objects/ui/Transition.tscn")
const obj_camera = preload("res://objects/PlayerCamera.tscn")
const obj_player = preload("res://objects/Player.tscn")
const obj_countdown = preload("res://objects/ui/Countdown.tscn")
const obj_hud = preload("res://objects/ui/HUD.tscn")
const obj_curseselect = preload("res://objects/ui/CardSelection.tscn")
const obj_outoftime = preload("res://objects/ui/OutOfTime.tscn")
const obj_levelclear = preload("res://objects/ui/LevelClear.tscn")
const obj_pausescreen = preload("res://objects/ui/PauseScreen.tscn")
const obj_pausescreen_nocurses = preload("res://objects/ui/NoCursesPauseScreen.tscn")
const obj_pausescreen_simple = preload("res://objects/ui/SimplePauseScreen.tscn")
const obj_ghostplayer = preload("res://objects/GhostPlayer.tscn")

const sound_gravity_up = preload("res://sounds/gravity_up.ogg")
const sound_gravity_down = preload("res://sounds/gravity_down.ogg")

export (NodePath) var path_player_spawn
export (NodePath) var path_orb
export (NodePath) var path_geometry
export (NodePath) var path_bgm

onready var player_spawn = get_node(path_player_spawn)
onready var orb = get_node(path_orb)
onready var geometry = get_node(path_geometry)
onready var bgm = get_node(path_bgm)
var player
var ghost_player
var camera
var hud
var gravity_timer
var ambience
var audio_gravity_flip

export (String) var level_slug
export (Vector2) var gravity = Vector2(0, 1)

onready var init_gravity = gravity

onready var respawning = false

var start_time
var pause_time
onready var time_spent_paused = 0

func flip_gravity():
	if Curses.get_current_curse() == Curses.GRAVITY_FLIPPING:
		gravity *= Vector2(0, -1)
		player.change_gravity(gravity)
		# Play the sound
		if gravity.y > 0: audio_gravity_flip.stream = sound_gravity_down
		else: audio_gravity_flip.stream = sound_gravity_up
		audio_gravity_flip.play()

func do_transition():
	var transition = obj_transition.instance()
	transition.transition_type = 0
	add_child(transition)

func init_camera():
	camera = obj_camera.instance()
	add_child(camera)
	camera.global_position = player_spawn.global_position
	camera.target_position = player_spawn.global_position
	camera.limit_right = geometry.get_level_width()
	camera.limit_bottom = geometry.get_level_height()

func init_player():
	player = obj_player.instance()
	player.camera = camera
	camera.global_position = player_spawn.global_position - (camera.CAMERA_SIZE/2)
	camera.reset_smoothing()
	player.set_level_bounds(geometry.get_level_width(), geometry.get_level_height())
	add_child(player)
	player.position = player_spawn.position
	player.change_gravity(gravity) # I don't know why this is necessary, but it is. Hmph.
	player.connect("died", self, "player_died")

func init_gravity_flipping():
	audio_gravity_flip = AudioStreamPlayer.new()
	audio_gravity_flip.bus = "SFX"
	add_child(audio_gravity_flip)
	gravity_timer = Timer.new()
	gravity_timer.connect("timeout", self, "flip_gravity")
	gravity_timer.wait_time = 2
	add_child(gravity_timer)
	gravity_timer.start()

func init_curses():
	var curses = obj_curseselect.instance()
	curses.curses = Levels.get_level_curses(level_slug)
	curses.level = level_slug
	curses.connect("done", self, "init_countdown")
	self.add_child(curses)
	get_tree().paused = true

func init_exit():
	orb.connect("reached", self, "level_complete")

func init_hud():
	hud = obj_hud.instance()
	hud.parent = self
	if Levels.level_has_curses(level_slug):
		hud.par = Levels.get_level_curse_par(level_slug, Curses.get_current_curse())
	add_child(hud)
	hud.set_health_visible(!Curses.get_current_curse() in [Curses.WEAK, Curses.JERK])
	hud.set_time_visible(Curses.get_current_curse() == Curses.TIME_LIMIT)
	hud.set_coins_visible(Curses.get_current_curse() == Curses.GREED)
	hud.connect_to_player(player)

func init_countdown():
	# We've just selected our curses; should we have coins in this level?
	if Curses.get_current_curse() == Curses.GREED:
		get_tree().call_group("coin", "fade_in")
	else: geometry.remove_coins()
	if Curses.get_current_curse() == Curses.CANNOT_LAND:
		geometry.make_floor_warning()
		get_tree().call_group("floor_indicator", "show")
	# The only reason to have a countdown is if we're on a time limit
	if Curses.get_current_curse() == Curses.TIME_LIMIT:
		var countdown = obj_countdown.instance()
		countdown.connect("start", self, "start_level")
		add_child(countdown)
	else: start_level()

func init_ambience():
	ambience = AudioStreamPlayer.new()
	ambience.bus = "AMB"
	ambience.stream = load("res://sounds/ambience/cave.ogg")
	ambience.pause_mode = Node.PAUSE_MODE_PROCESS
	add_child(ambience)
	ambience.play()

func init_gamemode() -> void:
	if GameSession.current_mode == GameSession.GAME_MODE.NORMAL:
		if Levels.level_has_curses(level_slug):
			init_curses()
		else:
			init_hud()
			start_time = OS.get_ticks_msec()
			# Play bgm, if assigned
			if bgm != null: bgm.play()
			get_tree().paused = false
	# This is speedrun mode: get started on the time-trial immediately!
	else:
		get_tree().paused = true
		Curses.set_current_curse(Curses.TIME_LIMIT)
		start_time = OS.get_ticks_msec()
		init_countdown()

func start_level():
	get_tree().paused = false
	init_hud()
	start_time = OS.get_ticks_msec()
	if Levels.level_has_music(level_slug) and not GameSession.is_speedrun():
		bgm.play()
	elif GameSession.is_speedrun():
		SpeedrunMusic.speedrun_start()
		GameSession.speedrun_start()
	# Ghost run?
	if GameProgress.ghost_run_exists(level_slug, Curses.get_current_curse()):
		spawn_ghost_player()

func pause():
	# Pause ingame/speedrun timer as appropriate
	pause_time = OS.get_ticks_msec()
	if GameSession.is_speedrun(): GameSession.speedrun_pause_timer()
	var simple = true if GameSession.is_speedrun() or GameProgress.new_game else false
	var no_curses = true if not Levels.level_has_curses(level_slug) else false
	var pause = obj_pausescreen_simple.instance() if simple else obj_pausescreen_nocurses.instance() if no_curses else obj_pausescreen.instance()
	pause.parent = self
	add_child(pause)

# So we can add time spent in conversation as "paused" according to the timer
func pause_timer():
	camera.set_zoom_actual(camera.ZOOM_NORMAL)
	pause_time = OS.get_ticks_msec()

func unpause():
	camera.reset_zoom()
	if GameSession.is_speedrun(): GameSession.speedrun_resume_timer()
	var unpause_time = OS.get_ticks_msec()
	time_spent_paused += unpause_time - pause_time

func get_current_time():
	var current_time = OS.get_ticks_msec()
	return current_time - start_time - time_spent_paused

func get_total_coins():
	var coins = get_tree().get_nodes_in_group("coin")
	return coins.size()

func get_collected_coins():
	var coins = get_tree().get_nodes_in_group("coin")
	var total = 0
	for next in coins:
		if next.collected: total += 1
	return total

func level_complete():
	# Are we cursed?
	if Curses.get_current_curse() == Curses.GREED:
		if get_collected_coins() != get_total_coins():
			return
	# Are we speedrunning?
	if GameSession.is_speedrun():
		GameSession.set_level_finished(level_slug, get_current_time())
		# Did we just beat the last level?
		if Levels.get_level_next(level_slug) == null:
			GameSession.speedrun_finish()
			SpeedrunMusic.speedrun_finish()
		else:
			GameSession.speedrun_pause_timer()
	# Everyone shut up!
	for next in [bgm, ambience]:
		if next != null: next.stop()
	camera.set_zoom_target(0.05)
	var clear = obj_levelclear.instance()
	clear.level = level_slug
	clear.curse = Curses.get_current_curse()
	clear.clear_time = get_current_time()
	clear.ghost_recording = player.ghost_recording
	add_child(clear)

func player_died():
	if !respawning:
		respawning = true
		var timer = Timer.new()
		timer.wait_time = 2
		timer.connect("timeout", self, "respawn_player")
		timer.one_shot = true
		add_child(timer)
		timer.start()

func respawn_player():
	gravity = init_gravity
	remove_child(gravity_timer)
	gravity_timer.queue_free()
	init_gravity_flipping()
	player.queue_free()
	init_player()
	start_time = OS.get_ticks_msec()
	time_spent_paused = 0
	respawning = false
	var resetables = get_tree().get_nodes_in_group("resetable")
	for next in resetables:
		next.reset()
	# Make camera snap back to player
	camera.global_position = player_spawn.global_position - (camera.CAMERA_SIZE/2)
	camera.reset_smoothing()
	hud.connect_to_player(player)
	# Reset the ghost player, if there is one
	if ghost_player != null:
		ghost_player.reset()

func spawn_ghost_player() -> void:
	ghost_player = obj_ghostplayer.instance()
	add_child(ghost_player)
	ghost_player.position = player_spawn.position
	ghost_player.ghost_recording = GameProgress.load_ghost_run(level_slug, Curses.get_current_curse())

func fade_out_ambience() -> void:
	var tween = Tween.new()
	tween.pause_mode = PAUSE_MODE_PROCESS
	add_child(tween)
	tween.interpolate_property(ambience, "volume_db", 0, -50, 5.0, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()

func _input(event):
	if event is InputEvent:
		if event.is_action_pressed("pause"):
			get_tree().set_input_as_handled()
			pause()
#		if event.is_action_pressed("cheat"):
#			level_complete()

func _process(delta):
	# Are we out of time?
	if Curses.get_current_curse() == Curses.TIME_LIMIT and not GameSession.is_speedrun():
		var par = Levels.get_level_curse_par(level_slug, Curses.get_current_curse())
		if get_current_time() > par and !respawning:
			var outoftime = obj_outoftime.instance()
			add_child(outoftime)
			player.die()
			respawning = true

func _ready():
	Curses.set_current_curse(-1)
	do_transition()
	init_camera()
	init_player()
	init_gravity_flipping()
	init_gamemode()
	init_exit()
	init_ambience()
	
