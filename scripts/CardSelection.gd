extends CanvasLayer

const texture_cursor = preload("res://sprites/ui/cursor.png")

const obj_card = preload("res://objects/ui/CurseCard.tscn")

onready var hbox_cards = $Center/HBox_Cards
onready var label_name = $Offset/Label_CurseName
onready var label_description = $Offset/Label_Description
onready var label_jerk = $Offset/Label_Jerk
onready var vbox_devsbest = $Offset/HBox/VBox_DevsBest
onready var vbox_yourbest = $Offset/HBox/VBox_YourBest
onready var label_devsbest = $Offset/HBox/VBox_DevsBest/Value
onready var label_yourbest = $Offset/HBox/VBox_YourBest/Value
onready var audio_move = $Audio_Move
onready var audio_select = $Audio_Select
onready var audio_start = $Audio_Start
onready var anim_player = $AnimationPlayer
onready var tween = $Tween

var curses = [Curses.GREED, Curses.TIME_LIMIT, Curses.GRAVITY_FLIPPING]

var selected_index = 0
onready var taking_input : bool = false

var level

signal done

func make_cards():
	var cards_made = []
	for next_curse in curses:
		var card = obj_card.instance()
		card.curse = next_curse
		hbox_cards.add_child(card)
		var curse_slug = Curses.get_curse_slug(next_curse)
		var cleared = GameProgress.is_curse_cleared(level, curse_slug)
		card.set_star_visible(cleared)
		cards_made.append(card)
	# Animate the cards appearing
	yield(get_tree().create_timer(0.5), "timeout")
	for current_card in cards_made:
		current_card.appear()
		yield(get_tree().create_timer(0.05), "timeout")

func time_to_string(time : int):
	var msec = time % 1000
	var sec = int(floor(time / 1000.0)) % 60
	var minute = floor(time / 60000.0)
	return "%02d:%02d.%03d" % [minute, sec, msec]

func update_hover():
	for current_card in hbox_cards.get_children():
		current_card.dehovered()
	hbox_cards.get_child(selected_index).hovered()
	var which_curse = hbox_cards.get_child(selected_index).curse
	label_name.text = Curses.get_curse_name(which_curse)
	label_description.text = Curses.get_curse_description(which_curse)
	label_jerk.visible = !Levels.does_curse_count(level, which_curse)
	# Display times if the level's already beaten
	if GameProgress.is_curse_cleared(level, Curses.get_curse_slug(which_curse)):
		label_yourbest.text = time_to_string(GameProgress.get_best_time_for_curse(level, Curses.get_curse_slug(which_curse)))
		vbox_yourbest.show()
		if Levels.get_devs_best_for_curse(level, which_curse) != -1:
			label_devsbest.text = time_to_string(Levels.get_devs_best_for_curse(level, which_curse))
			vbox_devsbest.show()
		else:
			vbox_devsbest.hide()
	else:
		vbox_yourbest.hide()
		vbox_devsbest.hide()

func _unhandled_input(event):
	if not taking_input: return
	if event.is_action_pressed("left"):
		selected_index -= 1
		if selected_index < 0:
			selected_index = hbox_cards.get_child_count() - 1
		update_hover()
		audio_move.play()
	elif event.is_action_pressed("right"):
		selected_index += 1
		if selected_index >= hbox_cards.get_child_count():
			selected_index = 0
		update_hover()
		audio_move.play()
	elif event.is_action_pressed("jump") or event.is_action_pressed("ui_accept"):
		Input.action_release("jump")
		var which_curse = hbox_cards.get_child(selected_index).curse
		Curses.set_current_curse(which_curse)
		done()
	
func done():
	taking_input = false
	anim_player.play("disappear")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "appear":
		taking_input = true
	if anim_name == "disappear":
		queue_free()

func game_start() -> void:
	emit_signal("done")

func _ready():
	make_cards()
	update_hover()
	anim_player.play("appear")
	tween.interpolate_property(label_jerk, "modulate", Color.darkred, Color.pink, 1.0, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	tween.interpolate_property(label_jerk, "modulate", Color.pink, Color.darkred, 1.0, Tween.TRANS_SINE, Tween.EASE_IN_OUT, 1.0)
	tween.repeat = true
	tween.start()
