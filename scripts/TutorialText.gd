extends Area2D

export (String, MULTILINE) var message

var target_alpha = 0
var actual_alpha = 0

onready var life = 5
onready var showing = false

onready var label = $CanvasLayer/Label

func _body_entered(body):
	if body.is_in_group("player") and life > 0:
		target_alpha = 1
		showing = true

func _body_exited(body):
	if body.is_in_group("player"):
		target_alpha = 0

func _process(delta):
	actual_alpha = lerp(actual_alpha, target_alpha, delta)
	label.modulate.a = actual_alpha
	if showing: life -= delta
	if life <= 0: target_alpha = 0

func _ready():
	label.text = message