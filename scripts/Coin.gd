extends Area2D

onready var sprite = $Sprite
onready var sprite_sparkle = $Sprite_Sparkle
onready var audio = $Audio_Pickup
onready var glow = $Glow
onready var tween = $Tween

onready var collected = false

onready var anim_index = 0

func fade_in() -> void:
	sprite.modulate = Color(1.0, 1.0, 1.0, 0.0)
	tween.interpolate_property(sprite, "modulate", Color(1.0, 1.0, 1.0, 0.0), Color.white, 0.25, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()
	visible = true

func _process(delta):
	anim_index += delta
	sprite.frame = int(anim_index * 10) % sprite.hframes
	if collected and anim_index < 0.2:
		sprite_sparkle.frame = int(anim_index * 20)
	else:
		sprite_sparkle.hide()

func _body_entered(body):
	if body.is_in_group("player"):
		if !collected:
			sprite.hide()
			sprite_sparkle.show()
			audio.play()
			glow.visible = false
			collected = true
			anim_index = 0

func reset():
	collected = false
	sprite.show()
	sprite_sparkle.hide()
	glow.visible = true

func _enter_tree() -> void:
	hide() # Only appear when the player selects the greed curse
