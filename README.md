# Hijinx

A game about platforming, curses, and turtles. Made for the third Godot Wild Jam.

Code & art by John Gabriel
Music by Kevin MacLeod